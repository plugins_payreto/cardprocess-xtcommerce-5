 ## 1.0.0 - 2017-09-19 - shop version compatible xtcommerce 5.0.07
* initial plugins.

## 1.0.0 - 2017-10-13 - shop version compatible xtcommerce 5.0.07
* Disable Klarna in backend configurations - PMC-2003
* Rename wrong variable name - PMC-2003

## 1.0.01 - 2017-12-20 - shop version compatible xtcommerce 5.0.07
* add amount validation on easycredit confirmation page - PMC-2306

## 1.0.02 - 2018-01-04 - shop version compatible xtcommerce 5.0.07
* change maximum amount of easyCredit to 5,000 EUR - PMC-2321