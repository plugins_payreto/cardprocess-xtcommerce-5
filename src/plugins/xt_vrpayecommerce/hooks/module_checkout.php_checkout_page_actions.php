<?php
/**
 * VR pay eCommerce - XTC4
 *
 * @copyright Copyright (c) 2015 VR pay eCommerce
 * @author    VR pay eCommerce <www.vr-epay.info>
 * @package   Vrpayecommerce/Hooks
 * @located   at   hooks/
 */

if (isset($_GET['error_vrpayecommerce_payment'])) {
    $info->_addInfo(constant($_GET['error_vrpayecommerce_payment']));
}
