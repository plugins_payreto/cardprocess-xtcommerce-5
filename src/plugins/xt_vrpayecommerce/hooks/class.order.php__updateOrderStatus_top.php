<?php
/**
 * VR pay eCommerce - XTC4
 *
 * @copyright Copyright (c) 2015 VR pay eCommerce
 * @author    VR pay eCommerce <www.vr-epay.info>
 * @package   Vrpayecommerce/Hooks
 * @located   at   hooks/
 */

require_once _SRV_WEBROOT."plugins/xt_vrpayecommerce/classes/class.xt_vrpayecommerce.php";
require_once _SRV_WEBROOT."plugins/xt_vrpayecommerce/classes/class.opp.php";

$payment = new xt_vrpayecommerce();
$action = '';

$payment->addLogVrpayecommerce('start update order');

if ($status == constant('XT_VRPAYECOMMERCE_STATUS_IR')) {
    $action = 'IR';
}
if ($status == constant('XT_VRPAYECOMMERCE_STATUS_ACCEPT')) {
    $action = 'CP';
}
if ($status == constant('XT_VRPAYECOMMERCE_STATUS_RF')) {
    $action = 'RF';
}

$payment->addLogVrpayecommerce('get action = ', $action);

if ($action && $trigger != 'user' && $trigger != 'IPN') {
    $order_record = $db->Execute(
        "SELECT payment_method, currency, amount, unique_id, payment_type
        FROM ".DB_PREFIX."_payment_vrpayecommerce_orders WHERE orders_id=?",
        array((int)$this->oID)
    );

    $payment->addLogVrpayecommerce('get order_record = ', $order_record);
    $payment->payment_method = $order_record->fields['payment_method'];
    $payment->addLogVrpayecommerce('get payment_method = ', $payment->payment_method);

    $transaction_parameters = $payment->getCredentials();
    $payment_type = $order_record->fields['payment_type'];
    $payment->addLogVrpayecommerce('get payment_type = ', $payment_type);
    $payment->addLogVrpayecommerce('get action = ', $payment_type);
    $reference_id = $order_record->fields['unique_id'];

    // process capture and refund
    if (($payment_type == 'PA' && $action == 'CP')
        || ($payment_type == 'CP' && $action == 'RF')
        || ($payment_type == 'DB' && $action == 'RF')
    ) {
        if ($payment->isMultiChannel()) {
            $transaction_parameters['channel_id'] = $transaction_parameters['channel_id_moto'];
        }

        $transaction_parameters['currency'] = $order_record->fields['currency'];
        $transaction_parameters['amount'] = $order_record->fields['amount'];
        $transaction_parameters['test_mode'] = $payment->getTestMode();
        $transaction_parameters['payment_type'] = $action;
        $payment->addLogVrpayecommerce('get transaction_parameters = ', $transaction_parameters);

        $update_order_result = vrpayecommerce_payment_core::backOfficeOperation($reference_id, $transaction_parameters);
        $payment->addLogVrpayecommerce('get update_order_result = ', $update_order_result);

        if (!$update_order_result['is_valid']) {
            $error_message = constant($update_order_result['response']);
            $callback_id = 'VR pay eCommerce';
            $callback_message = $update_order_result['response'];
            if ($action == 'CP') {
                $status = constant('XT_VRPAYECOMMERCE_STATUS_PA');
            }
            if ($action == 'RF') {
                $status = constant('XT_VRPAYECOMMERCE_STATUS_ACCEPT');
            }
            $send_email='false';
            $payment->addLogVrpayecommerce('error update_order_result = ', $update_order_result['response']);
        } else {
            $result_code = $update_order_result['response']["result"]["code"];
            $transaction_result = vrpayecommerce_payment_core::getTransactionResult($result_code);
            $payment->addLogVrpayecommerce('get transaction_result_code = ', $transaction_result);

            if ($transaction_result == 'ACK') {
                $db->Execute(
                    "UPDATE ".DB_PREFIX."_payment_vrpayecommerce_orders SET payment_type = '".$action."'
                    WHERE orders_id =?",
                    array((int)$this->oID)
                );
                $callback_id = 'VR pay eCommerce';
                $callback_message = 'Success';
                $send_email='true';
            } else {
                $callback_id = 'VR pay eCommerce';
                $callback_message = 'Failed';
                if ($action == 'CP') {
                    $status = constant('XT_VRPAYECOMMERCE_STATUS_PA');
                }
                if ($action == 'RF') {
                    $status = constant('XT_VRPAYECOMMERCE_STATUS_ACCEPT');
                }
                $send_email='false';
            }
        }
    }

    // process get status
    if ($payment_type == 'IR' && $action == 'IR') {
        $transaction_parameters['test_mode'] = $payment->getTestMode();

        $payment->addLogVrpayecommerce('get transaction_parameters = ', $transaction_parameters);

        $update_status_result = vrpayecommerce_payment_core::updateStatus($reference_id, $transaction_parameters);
        $payment->addLogVrpayecommerce('get update_status_result = ', $update_status_result);

        if (!$update_status_result['is_valid']) {
            $error_message = constant($update_status_result['response']);
            $success = false;
            $payment->addLogVrpayecommerce('error update_status_result = ', $error_message);
        } else {
            $xml_result = simplexml_load_string($update_status_result['response']);
            $payment->addLogVrpayecommerce('get transaction_result = ', $xml_result);
            $return_code = (string) $xml_result->Result->Transaction->Processing->Return['code'];

            $transaction_result = vrpayecommerce_payment_core::getTransactionResult($return_code);
            $payment->addLogVrpayecommerce('get transaction_result = ', $transaction_result);

            if ($transaction_result == 'ACK') {
                $success = true;
                if (vrpayecommerce_payment_core::isSuccessReview($return_code)) {
                    $payment_status = 'IR';
                }
                $payment_code = (string) $xml_result->Result->Transaction->Payment['code'];
                $payment_status = substr($payment_code, -2);
                $payment->addLogVrpayecommerce('get payment_status = ', $payment_status);
            } elseif ($transaction_result == 'NOK') {
                $success = false;
            } else {
                $success = false;
            }
        }

        $payment->addLogVrpayecommerce('is update order success = ', $success);
        if ($success) {
            $db->Execute(
                "UPDATE ".DB_PREFIX."_payment_vrpayecommerce_orders SET payment_type = '".$payment_status."'
                WHERE orders_id=?",
                array((int)$this->oID)
            );
            $callback_id = 'VR pay eCommerce';
            $callback_message = 'Success';
            if ($payment_status == 'PA') {
                $status = constant('XT_VRPAYECOMMERCE_STATUS_PA');
            }
            if ($payment_status == 'DB') {
                $status = constant('XT_VRPAYECOMMERCE_STATUS_ACCEPT');
            }
            $payment->addLogVrpayecommerce('update order status  = ', $status);
            $send_email='true';
        } else {
            $callback_id = 'VR pay eCommerce';
            if ($error_message) {
                $callback_message = $error_message;
            } else {
                $callback_message = 'Failed';
            }
            $send_email='false';
        }
    }
}
