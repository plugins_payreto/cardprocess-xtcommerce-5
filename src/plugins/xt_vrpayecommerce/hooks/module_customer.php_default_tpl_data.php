<?php
/**
 * VR pay eCommerce - XTC4
 *
 * @copyright Copyright (c) 2015 VR pay eCommerce
 * @author    VR pay eCommerce <www.vr-epay.info>
 * @package   Vrpayecommerce/Hooks
 * @located   at   hooks/
 */

require_once _SRV_WEBROOT."plugins/xt_vrpayecommerce/classes/class.xt_vrpayecommerce.php";

$payment = new xt_vrpayecommerce();
if ($payment->isRecurringActive()) {
    $tpl = 'account.html';
    $template->getTemplatePath($tpl, 'xt_vrpayecommerce', '', 'plugin');
}
