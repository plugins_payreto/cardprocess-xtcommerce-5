<?php
/**
 * VR pay eCommerce - XTC4
 *
 * @copyright Copyright (c) 2015 VR pay eCommerce
 * @author    VR pay eCommerce <www.vr-epay.info>
 * @package   Vrpayecommerce/Hooks
 * @located   at   hooks/
 */
require_once _SRV_WEBROOT."plugins/xt_vrpayecommerce/classes/class.xt_vrpayecommerce.php";
$payment = new xt_vrpayecommerce();
$disable_easycredit = false;

$is_amount_allowed = true;
if (!$payment->isAmountAllowed()) {
    $is_amount_allowed = false;
    $disable_easycredit = true;
}

$is_gender_not_empty = true;
if (!$payment->isGenderNotEmpty()) {
    $is_gender_not_empty = false;
    $disable_easycredit = true;
}

$is_date_of_birth_valid = true;
if (!$payment->isDateOfBirthValid()) {
    $is_date_of_birth_valid = false;
    $disable_easycredit = true;
}

$is_date_of_birth_lower_than_today = true;
if (!$payment->isDateOfBirthLowerThanToday()) {
    $is_date_of_birth_lower_than_today = false;
    $disable_easycredit = true;
}

$is_billing_equal_shipping = true;
if (!$payment->isBillingEqualShipping()) {
    $is_billing_equal_shipping = false;
    $disable_easycredit = true;
}

$sort_number = array(
    @constant('XT_VRPAYECOMMERCE_CCSAVED_SORT'),
    @constant('XT_VRPAYECOMMERCE_CC_SORT'),
    @constant('XT_VRPAYECOMMERCE_DC_SORT'),
    @constant('XT_VRPAYECOMMERCE_DDSAVED_SORT'),
    @constant('XT_VRPAYECOMMERCE_DD_SORT'),
    @constant('XT_VRPAYECOMMERCE_EPS_SORT'),
    @constant('XT_VRPAYECOMMERCE_GIROPAY_SORT'),
    @constant('XT_VRPAYECOMMERCE_IDEAL_SORT'),
    @constant('XT_VRPAYECOMMERCE_KLARNAPAYLATER_SORT'),
    @constant('XT_VRPAYECOMMERCE_KLARNASLICEIT_SORT'),
    @constant('XT_VRPAYECOMMERCE_MASTERPASS_SORT'),
    @constant('XT_VRPAYECOMMERCE_PAYPALSAVED_SORT'),
    @constant('XT_VRPAYECOMMERCE_PAYPAL_SORT'),
    @constant('XT_VRPAYECOMMERCE_KLARNAOBT_SORT'),
    @constant('XT_VRPAYECOMMERCE_SWISSPOSTFINANCE_SORT'),
    @constant('XT_VRPAYECOMMERCE_PAYDIREKT_SORT'),
    @constant('XT_VRPAYECOMMERCE_EASYCREDIT_SORT')
);

asort($sort_number);
$sort_number = array_unique($sort_number);

$tpl_data['is_amount_allowed'] = $is_amount_allowed;
$tpl_data['is_gender_not_empty'] = $is_gender_not_empty;
$tpl_data['is_date_of_birth_valid'] = $is_date_of_birth_valid;
$tpl_data['is_date_of_birth_lower_than_today'] = $is_date_of_birth_lower_than_today;
$tpl_data['is_billing_equal_shipping'] = $is_billing_equal_shipping;
$tpl_data['disable_easycredit'] = $disable_easycredit;
$tpl_data['is_recurring'] = @constant('XT_VRPAYECOMMERCE_GENERAL_RECURRING');
$tpl_data['is_customer'] = $_SESSION['customer']->customer_info['account_type'] == 0;
$tpl_data['sort_number'] = $sort_number;

$payment->addLogVrpayecommerce('is error easycredit parameter = ', $tpl_data);
