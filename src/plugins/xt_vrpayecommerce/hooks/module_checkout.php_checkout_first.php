<?php

defined('_VALID_CALL') or die('Direct Access is not allowed.');

global $xtPlugin, $page;

if (in_array($page->page_action, array('shipping', 'payment', 'confirmation'))) {
    unset($_SESSION['last_order_id']);
}

if (!empty($_POST['selected_payment_vrpayecommerce']) && $_POST['selected_payment'] == 'xt_vrpayecommerce' &&
    USER_POSITION == 'store' &&
    isset($xtPlugin->active_modules['xt_vrpayecommerce'])) {
    $_POST['selected_payment'] = $_POST['selected_payment_vrpayecommerce'];
}
