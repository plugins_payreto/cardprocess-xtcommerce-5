<?php
/**
 * VR pay eCommerce - XTC4
 *
 * @copyright Copyright (c) 2015 VR pay eCommerce
 * @author    VR pay eCommerce <www.vr-epay.info>
 * @package   Vrpayecommerce/Hooks
 * @located   at   hooks/
 */

$frame_url = $payment_module_data->IFRAME_URL;
$psp_target = $frame_url;

$checkout_data['psp_target'] = $psp_target;
