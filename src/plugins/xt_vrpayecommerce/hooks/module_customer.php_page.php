<?php
/**
 * VR pay eCommerce - XTC4
 *
 * @copyright Copyright (c) 2015 VR pay eCommerce
 * @author    VR pay eCommerce <www.vr-epay.info>
 * @package   Vrpayecommerce/Hooks
 * @located   at   hooks/
 */

require_once _SRV_WEBROOT."plugins/xt_vrpayecommerce/classes/class.xt_vrpayecommerce.php";
require_once _SRV_WEBROOT."plugins/xt_vrpayecommerce/classes/class.opp.php";
require_once _SRV_WEBROOT."plugins/xt_vrpayecommerce/classes/class.version_tracker.php";

$payment = new xt_vrpayecommerce();
// view page payment information

$payment->addLogVrpayecommerce('get page_action = ', $page->page_action);
if ($page->page_action == 'payment_information') {
    if (!$_SESSION['registered_customer']) {
        $payment->addLogVrpayecommerce('is not registered user');
        $xtLink->_redirect($xtLink->_link(array('page'=>'customer', 'paction'=>'login')));
    }
    $brotkrumen->_addItem($xtLink->_link(array('page'=>'customer')), TEXT_ACCOUNT);
    $brotkrumen->_addItem(
        $xtLink->_link(array('page'=>'customer', 'paction'=>'payment_information')),
        FRONTEND_MC_INFO
    );

    // process set default
    if (isset($_POST['set_default'])) {
        $payment->payment_method = $_POST['selected_payment'];
        $payment->updateDefaultRegistration($_POST['id']);
    }

    $is_recurring_active = $payment->isRecurringActive();
    $is_ccsaved_active = constant('XT_VRPAYECOMMERCE_CCSAVED_ACTIVATE');
    $is_ddsaved_active = constant('XT_VRPAYECOMMERCE_DDSAVED_ACTIVATE');
    $is_paypalsaved_active = constant('XT_VRPAYECOMMERCE_PAYPALSAVED_ACTIVATE');

    $tpl_data['is_recurring_active'] = $is_recurring_active;
    $tpl_data['is_ccsaved_active'] = $is_ccsaved_active;
    $tpl_data['is_ddsaved_active'] = $is_ddsaved_active;
    $tpl_data['is_paypalsaved_active'] = $is_paypalsaved_active;

    if ($is_recurring_active) {
        if ($is_ccsaved_active) {
            $payment->payment_method = 'CCSAVED';
            $tpl_data['cc_payment_account'] = $payment->getRegistrations();
        }
        if ($is_ddsaved_active) {
            $payment->payment_method = 'DDSAVED';
            $tpl_data['dd_payment_account'] = $payment->getRegistrations();
        }
        if ($is_paypalsaved_active) {
            $payment->payment_method = 'PAYPALSAVED';
            $tpl_data['paypal_payment_account'] = $payment->getRegistrations();
        }
    }

    if (isset($_GET['success'])) {
        $tpl_data['success'] = $_GET['success'];
    }
    if (isset($_GET['error'])) {
        $tpl_data['error'] = constant($_GET['error']);
    }
    $template = new Template();
    $tpl = 'payment_information.html';
    $template->getTemplatePath($tpl, 'xt_vrpayecommerce', '', 'plugin');
    $payment->addLogVrpayecommerce('get tpl_data = ', $tpl_data);
    $page_data = $template->getTemplate('smarty', $tpl, $tpl_data);
}

// view page save payment information
if ($page->page_action == 'register_payment') {
    if (!$_SESSION['registered_customer']) {
        $payment->addLogVrpayecommerce('is not registered user');
        $xtLink->_redirect($xtLink->_link(array('page'=>'customer', 'paction'=>'login')));
    }
    $brotkrumen->_addItem($xtLink->_link(array('page'=>'customer')), TEXT_ACCOUNT);
    $brotkrumen->_addItem(
        $xtLink->_link(
            array('page'=>'customer','paction'=>'payment_information', 'conn'=>'SSL')
        ),
        FRONTEND_MC_INFO
    );
    $brotkrumen->_addItem(
        $xtLink->_link(
            array('page'=>'customer', 'paction'=>'register_payment', 'conn'=>'SSL')
        ),
        FRONTEND_MC_SAVE
    );

    $payment->payment_method = $_REQUEST['selected_payment'];
    $payment->addLogVrpayecommerce('get payment_method = ', $payment->payment_method);

    $checkout_result = $payment->getCheckoutRecurring();

    if (!$checkout_result['is_valid']) {
        $payment->addLogVrpayecommerce('get error_message = ', $checkout_result['response']);
        $payment->redirectCustomerError($checkout_result['response']);
    } else {
        $payment->addLogVrpayecommerce('get checkout_id = ', $checkout_result['response']['id']);
        $payment->addLogVrpayecommerce('get payment_widget_url = ', $checkout_result['payment_widget_url']);

        $tpl_data = array(
            'cancel_url' =>  $xtLink->_link(array('page'=>'customer', 'paction'=>'payment_information', 'conn'=>'SSL')),
            'response_url' =>  $xtLink->_link(
                array(
                    'page'=>'customer',
                    'paction'=>'response_payment',
                    'params'=>'pm='.$payment->payment_method,
                    'conn'=>'SSL'
                )
            )
        );

        $tpl_data['lang'] = $payment->getLang();
        $tpl_data['test_mode'] = $payment->getTestMode();
        $tpl_data['payment_widget_url'] = $checkout_result['payment_widget_url'];
        $tpl_data['brand'] = $payment->getBrand();

        if ($payment->payment_method == 'PAYPALSAVED') {
            $tpl_data['redirect'] = true;
        }

        if (isset($_GET['error'])) {
            $tpl_data['error'] = constant($_GET['error']);
        }

        $template = new Template();
        $tpl = 'register_payment.html';
        $template->getTemplatePath($tpl, 'xt_vrpayecommerce', '', 'plugin');
        $payment->addLogVrpayecommerce('get tpl_data = ', $tpl_data);
        $page_data = $template->getTemplate('smarty', $tpl, $tpl_data);
    }
}

// view page change payment information
if ($page->page_action == 'change_payment') {
    if (!$_SESSION['registered_customer']) {
        $xtLink->_redirect($xtLink->_link(array('page'=>'customer', 'paction'=>'login')));
    }
    $brotkrumen->_addItem($xtLink->_link(array('page'=>'customer')), TEXT_ACCOUNT);
    $brotkrumen->_addItem(
        $xtLink->_link(
            array('page'=>'customer', 'paction'=>'payment_information', 'conn'=>'SSL')
        ),
        FRONTEND_MC_INFO
    );
    $brotkrumen->_addItem(
        $xtLink->_link(
            array('page'=>'customer', 'paction'=>'change_payment', 'conn'=>'SSL')
        ),
        FRONTEND_MC_CHANGE
    );

    $payment->payment_method = $_REQUEST['selected_payment'];
    $payment->addLogVrpayecommerce('get payment_method = ', $payment->payment_method);

    $id = $_REQUEST['id'];
    $payment->addLogVrpayecommerce('get id = ', $id);

    $checkout_result = $payment->getCheckoutRecurring($id);

    if (!$checkout_result['is_valid']) {
        $payment->addLogVrpayecommerce('get error_message = ', $checkout_result['response']);
        $payment->redirectCustomerError($checkout_result['response']);
    } else {
        $payment->addLogVrpayecommerce('get checkout_id = ', $checkout_result['response']['id']);
        $payment->addLogVrpayecommerce('get payment_widget_url = ', $checkout_result['payment_widget_url']);

        $tpl_data = array(
            'cancel_url' =>  $xtLink->_link(
                array('page'=>'customer', 'paction'=>'payment_information', 'conn'=>'SSL')
            ),
            'response_url' =>  $xtLink->_link(
                array(
                    'page'=>'customer',
                    'paction'=>'response_payment',
                    'params'=>'pm='.$payment->payment_method.'&recurringId='.$id,'conn'=>'SSL'
                )
            )
        );

        $tpl_data['lang'] = $payment->getLang();
        $tpl_data['test_mode'] = $payment->getTestMode();
        $tpl_data['payment_widget_url'] = $checkout_result['payment_widget_url'];
        $tpl_data['brand'] = $payment->getBrand();

        if ($payment->payment_method == 'PAYPALSAVED') {
            $tpl_data['redirect'] = true;
        }

        if (isset($_GET['error'])) {
            $tpl_data['error'] = constant($_GET['error']);
        }
        $template = new Template();
        $tpl = 'change_payment.html';
        $template->getTemplatePath($tpl, 'xt_vrpayecommerce', '', 'plugin');
        $payment->addLogVrpayecommerce('get tpl_data = ', $tpl_data);
        $page_data = $template->getTemplate('smarty', $tpl, $tpl_data);
    }
}

// view page delete payment information
if ($page->page_action == 'delete_payment') {
    if (!$_SESSION['registered_customer']) {
        $xtLink->_redirect($xtLink->_link(array('page'=>'customer', 'paction'=>'login')));
    }
    $brotkrumen->_addItem($xtLink->_link(array('page'=>'customer')), TEXT_ACCOUNT);
    $brotkrumen->_addItem(
        $xtLink->_link(
            array('page'=>'customer', 'paction'=>'payment_information', 'conn'=>'SSL')
        ),
        FRONTEND_MC_INFO
    );
    $brotkrumen->_addItem(
        $xtLink->_link(
            array('page'=>'customer', 'paction'=>'delete_payment', 'conn'=>'SSL')
        ),
        FRONTEND_MC_DELETE
    );

    $payment->payment_method = $_POST['selected_payment'];
    $payment->addLogVrpayecommerce('get Payment->payment_method = ', $payment->payment_method);

    $id = $_POST['id'];
    $payment->addLogVrpayecommerce('get id = ', $id);


    if (isset($_POST['do_delete'])) {
        $transaction_parameters = $payment->getCredentials();
        $transaction_parameters['test_mode'] = $payment->getTestMode();
        $payment->addLogVrpayecommerce('get transaction_parameters = ', $transaction_parameters);

        $reference_id = $payment->getReferenceId($id);
        $payment->addLogVrpayecommerce('get reference_id = ', $reference_id);

        $deregistration_response =
            vrpayecommerce_payment_core::deleteRegistration($reference_id, $transaction_parameters);
        $payment->addLogVrpayecommerce('get deregistration_response = ', $deregistration_response);

        if (!$deregistration_response['is_valid']) {
            $payment->redirectCustomerError($deregistration_response['response']);
        } else {
            $return_code = $deregistration_response['response']["result"]["code"];
            $return_message = vrpayecommerce_payment_core::getErrorIdentifier($return_code);
            $result = vrpayecommerce_payment_core::getTransactionResult($return_code);
            if ($result == "ACK") {
                $payment->deleteRecurring($id);
                $xtLink->_redirect(
                    $xtLink->_link(
                        array(
                            'page'=>'customer',
                            'paction'=>'payment_information',
                            'params'=>'success=delete'
                            )
                    )
                );
            } else {
                $tpl_data['error_message'] = constant($return_message);
            }
        }
    }

    $tpl_data['cancel_url'] =
        $xtLink->_link(array('page'=>'customer', 'paction'=>'payment_information', 'conn'=>'SSL'));
    $tpl_data['selected_payment'] = $payment->payment_method;
    $tpl_data['id'] = $id;

    $template = new Template();
    $tpl = 'delete_payment.html';
    $template->getTemplatePath($tpl, 'xt_vrpayecommerce', '', 'plugin');
    $page_data = $template->getTemplate('smarty', $tpl, $tpl_data);
}

// handle response
if ($page->page_action == 'response_payment') {
    if (!$_SESSION['registered_customer']) {
        $xtLink->_redirect($xtLink->_link(array('page'=>'customer', 'paction'=>'login')));
    }
    $checkout_id = isset($_GET['id']) ? $_GET['id'] : '';
    $payment->addLogVrpayecommerce('get checkout_id = ', $checkout_id);

    $payment_method = isset($_GET['pm']) ? $_GET['pm'] : '';
    $payment->addLogVrpayecommerce('get payment_method = ', $payment_method);

    $recurring_id = isset($_GET['recurringId']) ? $_GET['recurringId'] : '';
    $payment->addLogVrpayecommerce('get recurring_id = ', $recurring_id);

    if ($recurring_id) {
        $action = 'change';
    } else {
        $action = 'register';
    }
    $payment->addLogVrpayecommerce('get action = ', $action);

    $payment->payment_method = $payment_method;
    $payment->addLogVrpayecommerce('get payment_method = ', $payment->payment_method);

    $is_version_tracker_active = $payment->isVersionTrackerActive();
    $payment->addLogVrpayecommerce('is_version_tracker_active = ', $is_version_tracker_active);

    if ($is_version_tracker_active) {
        $version_tracker_response = version_tracker::sendVersionTracker($payment->getVersionData());
        $payment->addLogVrpayecommerce('get version_tracker_response = ', $version_tracker_response);
    }

    $transaction_parameters = $payment->getCredentials();
    $payment->addLogVrpayecommerce('get transaction_parameters = ', $transaction_parameters);

    $register_response = vrpayecommerce_payment_core::getPaymentStatus($checkout_id, $transaction_parameters);
    $payment->addLogVrpayecommerce('get register_response = ', $register_response);

    $error = '';

    if (!$register_response['is_valid']) {
        $error = $register_response['response'];
    } else {
        $return_code = $register_response['response']["result"]["code"];
        $return_message = vrpayecommerce_payment_core::getErrorIdentifier($return_code);
        $payment->addLogVrpayecommerce('get return_message = ', $return_message);

        $transaction_result = vrpayecommerce_payment_core::getTransactionResult($return_code);
        $payment->addLogVrpayecommerce('get transaction_result = ', $transaction_result);

        if ($transaction_result == "ACK") {
            // process Payment Success Recurring
            $transaction_parameters['amount'] = $payment->getRegisterAmount();
            $transaction_parameters['currency'] = $currency->code;

            if ($action == 'change') {
                $transaction_parameters['transaction_id'] = $register_response['response']['merchantTransactionId'];
            } else {
                $transaction_parameters['transaction_id'] = $payment->user_id;
            }
            $transaction_parameters['payment_recurring'] = 'INITIAL';
            $transaction_parameters['test_mode'] = $payment->getTestMode();
            $payment->addLogVrpayecommerce('get transaction_parameters = ', $transaction_parameters);

            // process PayPalSaved Recurring
            if ($payment->payment_method == "PAYPALSAVED") {
                $registration_id = $register_response['response']['id'];
                $payment->addLogVrpayecommerce('get registration_id = ', $registration_id);
                $transaction_parameters['payment_type'] = 'DB';
                $response = vrpayecommerce_payment_core::useRegistration($registration_id, $transaction_parameters);
                $payment->addLogVrpayecommerce('get response = ', $response);
                if ($response['is_valid']) {
                    $return_code = $response['response']["result"]["code"];
                    $return_message = vrpayecommerce_payment_core::getErrorIdentifier($return_code);
                    $paypal_response = vrpayecommerce_payment_core::getTransactionResult($return_code);
                    $payment->addLogVrpayecommerce('get paypal_response = ', $paypal_response);

                    if ($paypal_response == "ACK") {
                        $reference_id = $response['response']['id'];
                        $transaction_parameters['payment_type'] = 'RF';

                        $back_office_operation_response =
                            vrpayecommerce_payment_core::backOfficeOperation($reference_id, $transaction_parameters);
                        $payment->addLogVrpayecommerce(
                            'get back_office_operation_response = ',
                            $back_office_operation_response
                        );

                        $payment->saveRegistration($recurring_id, $registration_id, $register_response['response']);
                        $payment->addLogVrpayecommerce('done saveRegistration');
                    } elseif ($paypal_response == "NOK") {
                        $error = $return_message;
                    } else {
                        $error = 'ERROR_UNKNOWN';
                    }
                } else {
                    $error = $response['response'];
                }
            } else {
                // process PaymentSaved Recurring
                $reference_id = $register_response['response']['id'];
                $payment->addLogVrpayecommerce('get reference_id = ', $reference_id);

                $registration_id = $register_response['response']['registrationId'];
                $payment->addLogVrpayecommerce('get registration_id = ', $registration_id);

                $payment->addLogVrpayecommerce('get isMultiChannel = ', $payment->isMultiChannel());
                if ($payment->isMultiChannel()) {
                    $transaction_parameters['channel_id'] = $transaction_parameters['channel_id_moto'];
                    $payment->addLogVrpayecommerce('get channel_id = ', $transaction_parameters['channel_id']);
                }
                
                $payment->addLogVrpayecommerce('get getPaymentType = ', $payment->getPaymentType());
                if ($payment->getPaymentType() == 'PA') {
                    $transaction_parameters['payment_type'] = "CP";
                    $response =
                        vrpayecommerce_payment_core::backOfficeOperation($reference_id, $transaction_parameters);
                    $payment->addLogVrpayecommerce('get response = ', $response);
                    if ($response['is_valid']) {
                        $return_code = $response['response']["result"]["code"];
                        $return_message = vrpayecommerce_payment_core::getErrorIdentifier($return_code);
                        $transaction_result = vrpayecommerce_payment_core::getTransactionResult($return_code);
                        $payment->addLogVrpayecommerce('get transaction_result = ', $transaction_result);


                        if ($transaction_result == 'ACK') {
                            $reference_id = $response['response']['id'];
                        } elseif ($transaction_result == 'NOK') {
                            $error = $return_message;
                        } else {
                            $error = 'ERROR_UNKNOWN';
                        }
                    } else {
                        $error = $response['response'];
                    }
                }
                if (!$error) {
                    $transaction_parameters['payment_type'] = "RF";

                    $back_office_operation_response =
                        vrpayecommerce_payment_core::backOfficeOperation($reference_id, $transaction_parameters);
                    $payment->addLogVrpayecommerce(
                        'get back_office_operation_response = ',
                        $back_office_operation_response
                    );

                    $payment->saveRegistration($recurring_id, $registration_id, $register_response['response']);
                    $payment->addLogVrpayecommerce('done saveRegistration');
                }
            }
            if (!$error) {
                if ($action == 'change') {
                    $reference_id = $register_response['response']['merchantTransactionId'];
                    $payment->addLogVrpayecommerce('get reference_id = ', $reference_id);

                    vrpayecommerce_payment_core::deleteRegistration($reference_id, $transaction_parameters);
                    $payment->addLogVrpayecommerce('done deleteRegistration');
                }
                $xtLink->_redirect(
                    $xtLink->_link(
                        array('page'=>'customer', 'paction'=>'payment_information', 'params'=>'success='.$action)
                    )
                );
            }
        } elseif ($transaction_result == "NOK") {
            $error = $return_message;
        } else {
            $error = 'ERROR_UNKNOWN';
        }
    }

    // redirect error
    if ($error) {
        if ($payment->payment_method == 'PAYPALSAVED') {
            $xtLink->_redirect(
                $xtLink->_link(array('page'=>'customer', 'paction'=>'payment_information', 'params'=>'error='.$error))
            );
        } else {
            if ($action == 'register') {
                $xtLink->_redirect(
                    $xtLink->_link(
                        array(
                            'page'=>'customer',
                            'paction'=>'register_payment',
                            'params'=>'selected_payment='.$payment_method.'&error='.$error
                        )
                    )
                );
            } else {
                $xtLink->_redirect(
                    $xtLink->_link(
                        array(
                            'page'=>'customer',
                            'paction'=>'change_payment',
                            'params'=>'selected_payment='.$payment_method.'&id='.$recurring_id.'&error='.$error
                        )
                    )
                );
            }
        }
    }
}
