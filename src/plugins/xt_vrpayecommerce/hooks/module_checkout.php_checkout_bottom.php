<?php
/**
 * VR pay eCommerce - XTC4
 *
 * @copyright Copyright (c) 2015 VR pay eCommerce
 * @author    VR pay eCommerce <www.vr-epay.info>
 * @package   Vrpayecommerce/Hooks
 * @located   at   hooks/
 */

require_once _SRV_WEBROOT."plugins/xt_vrpayecommerce/classes/class.xt_vrpayecommerce.php";
require_once _SRV_WEBROOT."plugins/xt_vrpayecommerce/classes/class.opp.php";
require_once _SRV_WEBROOT."plugins/xt_vrpayecommerce/classes/class.version_tracker.php";

$payment = new xt_vrpayecommerce();
$smarty = new Smarty();

$payment->addLogVrpayecommerce('get page_action = ', $page->page_action);
if ($page->page_action == 'order_confirmation') {
    $brotkrumen->_addItem($xtLink->_link(array('page'=>'checkout')), TEXT_CHECKOUT);
    $brotkrumen->_addItem(
        $xtLink->_link(array('page'=>'checkout', 'paction'=>'order_confirmation')),
        FRONTEND_GENERAL_CONFIRM_ORDER
    );

    $products = $_SESSION['cart']->show_content ;
    $payment->addLogVrpayecommerce('get product = ', $products);
    
    $tmp = new Template;
    $selected_template = $tmp->selected_template;
    $tpl_data['cart'] = '';
    foreach ($products as $key => $value) {
        $tpl_data['cart'] .=
                "<tr>
                    <td class='center'>";

        if ($value['products_image'] == 'product:noimage.gif') {
            $tpl_data['cart'] .=
                    "<p class='box box-white box-hover'>
                    <img src='"._SYSTEM_BASE_URL._SRV_WEB._SRV_WEB_TEMPLATES.$selected_template."/img/no_image.png' 
                    style='width:50px;height:auto;'/></p>";
        } else {
            $product_image = explode(':', $value['products_image']);
            $tpl_data['cart'] .=
                    "<p class='box box-white box-hover'>
                    <img src='"._SYSTEM_BASE_URL._SRV_WEB."media/images/thumb/".$product_image[1]."'
                    style='width:50px;height:auto;'/></p>";
        }
            $tpl_data['cart'] .=
                    "</td>
                        <td>
                            <p class='product-name'>".$value['products_name']."</p>
                        </td>
                        <td class='right'>
                            <p>".$value['products_price']['formated']."</p>
                        </td>
                        <td class='right'>".$value['products_quantity']."</td>
                        <td class='bold right'>"
                        .$value['products_final_price']['formated']."</td>
                    </tr>";
    }
    
    $tpl_data['items_count'] = count($_SESSION['cart']->show_content);
    ;
    $tpl_data['sub_total'] = $_SESSION['cart']->content_total_physical['formated'];
    $tpl_data['sub_data']['products_price']['formated'] = $_SESSION['cart']->sub_content_total['formated'];
    $tpl_data['total'] = $_SESSION['cart']->total_physical['formated'];
    $tpl_data['cancel_url'] = $xtLink->_link(
        array(
                'page'=>'checkout',
                'paction'=>'cancel',
                'conn'=>'SSL'
            )
    );
    $tpl_data['success_url'] = $xtLink->_link(
        array(
                'page'=>'checkout',
                'paction'=>'validation',
                'conn'=>'SSL'
            )
    );
    
    $easycredit_payment_response = $payment->getEasycreditPaymentResponse();
    $payment->addLogVrpayecommerce('get easycredit payment response = ', $easycredit_payment_response);
    $tpl_data['servertoserver_txt'] = $easycredit_payment_response['resultDetails']['tilgungsplanText'];
    $tpl_data['servertoserver_amount'] = $easycredit_payment_response['amount'];
    $tpl_data['servertoserver_sum'] =
        $easycredit_payment_response['resultDetails']['ratenplan.zinsen.anfallendeZinsen'];
    $tpl_data['servertoserver_total'] = $easycredit_payment_response['resultDetails']['ratenplan.gesamtsumme'];
    $tpl_data['servertoserver_currency'] = $easycredit_payment_response['currency'];
    $tpl_data['servertoserver_link'] = $easycredit_payment_response['resultDetails']['vorvertraglicheInformationen'];
    $tpl_data['servertoserver_id'] = $easycredit_payment_response['id'];

    $_SESSION['servertoserver_id'] = $tpl_data['servertoserver_id'];
    $_SESSION['servertoserver_currency'] = $tpl_data['servertoserver_currency'];
    $_SESSION['servertoserver_amount'] = $tpl_data['servertoserver_amount'];
    $payment->addLogVrpayecommerce('get tpl_data = ', $tpl_data);

    $tpl = 'order_confirmation.html';
    $template->getTemplatePath($tpl, 'xt_vrpayecommerce', '', 'plugin');
}

if ($page->page_action == 'validation') {
        $link = $xtLink->_link(
            array(
                'page'=>'callback',
                'paction'=>'xt_vrpayecommerce',
                'conn'=>'SSL',
                'params' => 'confirm_order=1'
            )
        );
        $xtLink->_redirect($link);
}

if ($page->page_action == 'cancel') {
        $link = $xtLink->_link(
            array(
                'page'=>'checkout',
                'paction'=>'confirmation'
            )
        );
        unset($_SESSION['servertoserver_id']);
        unset($_SESSION['servertoserver_currency']);
        unset($_SESSION['servertoserver_amount']);
        $xtLink->_redirect($link);
}
