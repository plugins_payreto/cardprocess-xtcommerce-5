<?php
/**
 * VR pay eCommerce - XTC4
 *
 * @copyright Copyright (c) 2015 VR pay eCommerce
 * @author    VR pay eCommerce <www.vr-epay.info>
 * @package   Vrpayecommerce/Classes
 * @located   at   callback/
 */

defined('_VALID_CALL') or die('Direct Access is not allowed.');

require_once _SRV_WEBROOT."plugins/xt_vrpayecommerce/classes/class.xt_vrpayecommerce.php";
require_once _SRV_WEBROOT."plugins/xt_vrpayecommerce/classes/class.opp.php";
require_once _SRV_WEBROOT."plugins/xt_vrpayecommerce/classes/class.version_tracker.php";

/**
 * Plugin Main Process
 */
class callback_xt_vrpayecommerce extends callback
{
    /**
*
     *
 * @var object vrpayecommerce payment object
*/
    protected $payment;

    public function __construct()
    {
        $this->payment = new xt_vrpayecommerce();
    }

    /**
     * Plugin initial process.
     *
     * @return string | boolean
     */
    public function process()
    {
        global $filter, $xtLink, $order, $db;

        if (isset($_GET['do_checkout'])) {
            $this->renderCheckoutWidget();
        }

        if (isset($_GET['id']) || isset($_POST['registrationId'])) {
            $this->getResponse();
        }

        if (isset($_GET['confirm_order'])) {
            $this->processConfirmOrder();
        }

        return false;
    }

    /**
     * Proses capture and sending version tracker
     *
     * @return void
     */
    protected function processConfirmOrder()
    {
        if (isset($_SESSION['last_order_id'])) {
            $order_id = $_SESSION['last_order_id'];
        }
        $this->payment->addLogVrpayecommerce('get order_id = ', $order_id);
        $order = new order($order_id, $this->payment->user_id);
        $this->payment->addLogVrpayecommerce('get order = ', $order);
        $is_version_tracker_active = $this->payment->isVersionTrackerActive();
        $this->payment->addLogVrpayecommerce('is_version_tracker_active = ', $is_version_tracker_active);
        if ($is_version_tracker_active) {
            $version_tracker_response = version_tracker::sendVersionTracker($this->payment->getVersionData());
            $this->payment->addLogVrpayecommerce('get response from version tracker = ', $version_tracker_response);
        }

        if (isset($_SESSION['servertoserver_id'])) {
            $reference_id = $_SESSION['servertoserver_id'];
        }
        
        $this->payment->addLogVrpayecommerce('get reference_id = ', $reference_id);

        $transaction_data = $this->payment->getCredentials();
        if (isset($_SESSION['servertoserver_amount']) && isset($_SESSION['servertoserver_currency'])) {
            $transaction_data['amount'] = $_SESSION['servertoserver_amount'];
            $transaction_data['currency'] = $_SESSION['servertoserver_currency'];
        }
        $cartAmount = vrpayecommerce_payment_core::setNumberFormat(
            $_SESSION['cart']->total['plain']
        );
        if ($cartAmount != $_SESSION['servertoserver_amount']) {
            $this->redirectError('ERROR_GENERAL_CAPTURE_PAYMENT');
        }
        unset($_SESSION['servertoserver_id']);
        unset($_SESSION['servertoserver_currency']);
        unset($_SESSION['servertoserver_amount']);
        $transaction_data['test_mode'] = $this->payment->getTestMode();
        $transaction_data['payment_type'] = "CP";
        $this->payment->addLogVrpayecommerce('get transaction_data = ', $transaction_data);
        $payment_status = vrpayecommerce_payment_core::backOfficeOperation($reference_id, $transaction_data);
        $this->payment->addLogVrpayecommerce('get payment_status = ', $payment_status);
        if (!$payment_status['is_valid']) {
            $this->redirectError($payment_status['response']);
        } else {
            $code_result = $payment_status['response']["result"]["code"];
            $transaction_result = vrpayecommerce_payment_core::getTransactionResult($code_result);

            if ($transaction_result == "ACK") {
                $this->processPaymentSuccess($payment_status['response'], $order);
            } elseif ($transaction_result == "NOK") {
                $error_identifier = vrpayecommerce_payment_core::getErrorIdentifier($code_result);
                $this->redirectError($error_identifier);
            } else {
                $this->redirectError('ERROR_UNKNOWN');
            }
        }
    }

    /**
     * Render payment widget when checkout
     *
     * @return void
     */
    protected function renderCheckoutWidget()
    {
        global $xtLink;
        $this->payment->addLogVrpayecommerce('render checkout widget');
        $payment_widget_url = urldecode($_GET['payment_widget_url']);
        $this->payment->addLogVrpayecommerce('payment_widget_url = ', $payment_widget_url);
        $tpl_variables = array(
            'cancel_url' =>  $xtLink->_link(array('page'=>'checkout', 'paction'=>'payment', 'conn'=>'SSL')),
            'response_url' =>  $xtLink->_link(array('page'=>'callback', 'paction'=>'xt_vrpayecommerce', 'conn'=>'SSL'))
        );
        $tpl_variables['lang'] = $this->payment->getLang();
        $tpl_variables['test_mode'] = $this->payment->getTestMode();
        $tpl_variables['recurring'] = $this->payment->isRecurring();
        $tpl_variables['registrations'] = $this->payment->getRegistrations();
        $tpl_variables['redirect'] = $this->payment->isRedirect();
        $tpl_variables['payment_widget_url'] = $payment_widget_url;
        $tpl_variables['brand'] = $this->payment->getBrand();
        $tpl_variables['merchant_location'] = $this->payment->getMerchantLocation();
        $tpl_variables['is_cc_payment_method'] = $this->payment->isCcPaymentMethod($this->payment->payment_method);
        $this->payment->addLogVrpayecommerce('get tpl_variables = ', $tpl_variables);
        if ($this->payment->payment_method == 'PAYPALSAVED') {
            $tpl_file = 'paypalsaved.html';
        } else {
            $tpl_file = 'form_cp.html';
        }
        $this->payment->addLogVrpayecommerce('set tpl_file = ', $tpl_file);
        $template = new Template();

        $template->getTemplatePath($tpl_file, 'xt_vrpayecommerce', '', 'plugin');
        echo $template->getTemplate('xt_vrpayecommerce_form_smarty', $tpl_file, $tpl_variables);
    }

    /**
     * Get payment response from gateway
     *
     * @return void
     */
    protected function getResponse()
    {
        $this->payment->addLogVrpayecommerce('start get response from gateway');
        $order_id = $_SESSION['last_order_id'];
        $this->payment->addLogVrpayecommerce('order_id = ', $order_id);

        $is_version_tracker_active = $this->payment->isVersionTrackerActive();
        $this->payment->addLogVrpayecommerce('is_version_tracker_active = ', $is_version_tracker_active);

        if ($is_version_tracker_active) {
            $version_tracker_response = version_tracker::sendVersionTracker($this->payment->getVersionData());
            $this->payment->addLogVrpayecommerce('get response from version tracker = ', $version_tracker_response);
        }

        $checkout_id = $_GET['id'];
        $this->payment->addLogVrpayecommerce('get checkout_id = ', $checkout_id);

        $registration_id = '';
        if (isset($_POST['registrationId'])) {
            $registration_id = $_POST['registrationId'];
            $this->payment->addLogVrpayecommerce('get registration_id = ', $registration_id);
        }

        $order = new order($order_id, $this->payment->user_id);
        $this->payment->addLogVrpayecommerce('get order detail = ', $order);

        if ($registration_id && $this->payment->payment_method == 'PAYPALSAVED') {
            $this->processRegisteredPayPalRecurring($registration_id, $order);
        }
        $transaction_parameters = $this->payment->getCredentials();
        $this->payment->addLogVrpayecommerce('get transaction_parameters = ', $transaction_parameters);
       
        $payment_response = vrpayecommerce_payment_core::getPaymentStatus($checkout_id, $transaction_parameters);
        $this->payment->addLogVrpayecommerce('get payment_response = ', $payment_response);
        if (!$payment_response['is_valid']) {
            $this->redirectError($payment_response['response']);
        } else {
            $return_code = $payment_response['response']["result"]["code"];
            $return_message = vrpayecommerce_payment_core::getErrorIdentifier($return_code);
            $transaction_result = vrpayecommerce_payment_core::getTransactionResult($return_code);

            if ($transaction_result == "ACK") {
                $this->processPaymentSuccess($payment_response['response'], $order);
            } elseif ($transaction_result == "NOK") {
                $this->redirectError($return_message);
            } else {
                $this->redirectError('ERROR_UNKNOWN');
            }
        }
    }

    /**
     * Process paypal payment use stored data
     *
     * @return void
     */
    protected function processRegisteredPayPalRecurring($registration_id, $order)
    {
        $transaction_parameters = $_SESSION['vrpayecommerce']['transaction_data'];
        $transaction_parameters['payment_type'] = 'DB';
        $this->payment->addLogVrpayecommerce('start register paypal account');
        $this->payment->addLogVrpayecommerce('get transaction_parameters = ', $transaction_parameters);
        $this->payment->addLogVrpayecommerce('get registration_id = ', $registration_id);

        $debit_response = vrpayecommerce_payment_core::useRegistration($registration_id, $transaction_parameters);
        $this->payment->addLogVrpayecommerce('get registration result  = ', $debit_response);
        if ($debit_response['is_valid']) {
            $return_code = $debit_response['response']["result"]["code"];
            $return_message = vrpayecommerce_payment_core::getErrorIdentifier($return_code);
            $transaction_result = vrpayecommerce_payment_core::getTransactionResult($return_code);

            if ($transaction_result == "ACK") {
                $this->redirectSuccess($order, $debit_response['response']);
            } elseif ($transaction_result == "NOK") {
                $this->redirectError($return_message);
            } else {
                $this->redirectError('ERROR_UNKNOWN');
            }
        } else {
                $this->redirectError($debit_response['response']);
        }
    }

    /**
     * Process paypal payment after registration
     *
     * @return void
     */
    protected function processPaypalRecurring($payment_response, $order)
    {
        $this->payment->addLogVrpayecommerce('process paypal recurring');
        $registration_id = $payment_response['id'];
        $this->payment->addLogVrpayecommerce('get registration_id = ', $registration_id);
        $transaction_parameters = $this->payment->getCredentials();
        $transaction_parameters['amount'] = $_SESSION['vrpayecommerce']['transaction_data']['amount'];
        $transaction_parameters['currency'] = $_SESSION['vrpayecommerce']['transaction_data']['currency'];
        $transaction_parameters['transaction_id'] = $payment_response['merchantTransactionId'];
        $transaction_parameters['test_mode'] = $this->payment->getTestMode();
        $transaction_parameters['payment_recurring'] = 'INITIAL';
        $transaction_parameters['payment_type'] = 'DB';
        $this->payment->addLogVrpayecommerce('get transaction_parameters = ', $transaction_parameters);
        $debit_response = vrpayecommerce_payment_core::useRegistration($registration_id, $transaction_parameters);
        $this->payment->addLogVrpayecommerce('get debit_response = ', $debit_response);

        if ($debit_response['is_valid']) {
            $return_code = $debit_response['response']["result"]["code"];
            $return_message = vrpayecommerce_payment_core::getErrorIdentifier($return_code);
            $transaction_result = vrpayecommerce_payment_core::getTransactionResult($return_code);
            $this->payment->addLogVrpayecommerce('get transaction_result = ', $transaction_result);
           
            if ($transaction_result == "ACK") {
                $_SESSION['vrpayecommerce']['resultJson']['id'] = $debit_response['response']['id'];
                $this->payment->insertRegistration($registration_id, $payment_response);
                $this->redirectSuccess($order, $debit_response['response']);
            } elseif ($transaction_result == "NOK") {
                $this->redirectError($return_message);
            } else {
                $this->redirectError('ERROR_UNKNOWN');
            }
        } else {
            $this->redirectError($debit_response['response']);
        }
    }

    /**
     * Process payment when success
     *
     * @param  array  $payment_response
     * @param  object $order
     * @return void
     */
    protected function processPaymentSuccess($payment_response, $order)
    {
        $this->payment->addLogVrpayecommerce('process payment success');
        if ($this->payment->isRecurring()) {
            if ($this->payment->payment_method == 'PAYPALSAVED') {
                $this->processPaypalRecurring($payment_response, $order);
            } else {
                $registration_id = $payment_response['registrationId'];
                $this->payment->insertRegistration($registration_id, $payment_response);
            }
        }

        $this->redirectSuccess($order, $payment_response);
    }

    /**
     * Save the order when doing payment with vrpayecommerce
     *
     * @param  object $order
     * @param  array  $payment_response
     * @return void
     */
    protected function addVRpayOrder($order, $payment_response)
    {
        global $db;
        $this->payment->addLogVrpayecommerce('process add VR pay Order to database');
        $payment_code = $order->order_data['subpayment_code'];
        $this->payment->addLogVrpayecommerce('get payment_code = ', $payment_code);
        if (vrpayecommerce_payment_core::isSuccessReview($payment_response['result']['code'])) {
            $payment_type = 'IR';
        } else {
            $payment_type = $payment_response['paymentType'];
        }
        $this->payment->addLogVrpayecommerce('get payment_type = ', $payment_type);

        if ($payment_code == "DD" || $payment_code == "DDSAVED") {
            $db->Execute(
                "
                INSERT INTO ".DB_PREFIX."_payment_vrpayecommerce_orders
                (orders_id, payment_method, unique_id, amount, currency, mandate_id, mandate_date, payment_type)
                VALUES (
                    '".$order->oID."',
                    '".$payment_code."',
                    '".$payment_response['id']."',
                    '".$payment_response['amount']."',
                    '".$payment_response['currency']."',
                    '',
                    '',
                    '".$payment_type."'
                )
            "
            );
        } else {
            $db->Execute(
                "
                INSERT INTO ".DB_PREFIX."_payment_vrpayecommerce_orders
                (orders_id, payment_method, unique_id, amount, currency, payment_type)
                VALUES (
                    '".$order->oID."',
                    '".$payment_code."',
                    '".$payment_response['id']."',
                    '".$payment_response['amount']."',
                    '".$payment_response['currency']."',
                    '".$payment_type."'
                )
            "
            );
        }
    }

    /**
     * Redirect to success page
     *
     * @param  object $order
     * @param  array  $payment_response
     * @return void
     */
    protected function redirectSuccess($order, $payment_response)
    {
        global $xtLink, $db;
        $this->payment->addLogVrpayecommerce('process redirect success');
        $this->addVRpayOrder($order, $payment_response);

        $payment_method = $order->order_data['subpayment_code'];
        $payment_info['payment_name'] = constant('TEXT_PAYMENT_'.$payment_method);
        if (substr($payment_method, -5) == 'SAVED') {
            $payment_info['payment_name'] .= ' (Recurring)';
        }
        if (vrpayecommerce_payment_core::isSuccessReview($payment_response['result']['code'])) {
            $order_status = constant('XT_VRPAYECOMMERCE_STATUS_IR');
        } elseif ($payment_response['paymentType']=="PA") {
            $order_status = constant('XT_VRPAYECOMMERCE_STATUS_PA');
        } else {
            $order_status = constant('XT_VRPAYECOMMERCE_STATUS_ACCEPT');
        }
        $this->payment->addLogVrpayecommerce('get order_status = ', $order_status);
        $payment_info['transaction_id'] = $payment_response['merchantTransactionId'];

        $payment_info = serialize($payment_info);
        $this->payment->addLogVrpayecommerce('get payment_info = ', $payment_info);
        $record = array('orders_data' => $payment_info);
        $this->payment->addLogVrpayecommerce('get record = ', $record);
        $db->AutoExecute(TABLE_ORDERS, $record, 'UPDATE', "orders_id=". (int) $_SESSION['last_order_id']."");
        $this->payment->addLogVrpayecommerce('process update order database');

        $order->_sendOrderMail($_SESSION['last_order_id']);
        $this->payment->addLogVrpayecommerce('process send order mail');
        $order->_updateOrderStatus($order_status, '', 'true', 'true', 'IPN', 'VR pay eCommerce');
        $this->payment->addLogVrpayecommerce('process update order backend & frontend');

        if (isset($_SESSION['last_order_id'])) {
            $_SESSION['success_order_id'] = $_SESSION['last_order_id'];
        }

        unset($_SESSION['last_order_id']);
        unset($_SESSION['selected_shipping']);
        unset($_SESSION['selected_payment']);
        unset($_SESSION['conditions_accepted']);
        $_SESSION['cart']->_resetCart();
        $this->payment->addLogVrpayecommerce('reset session');
        $link  = $xtLink->_link(array('page'=>'checkout', 'paction'=>'success'));
        $xtLink->_redirect($link);
        $this->payment->addLogVrpayecommerce('redirect success page');
    }

    /**
     * Redirect to error page
     *
     * @param  string $return_message
     * @return void
     */
    protected function redirectError($return_message)
    {
        global $xtLink;

        $link = $xtLink->_link(
            array(
                'page' => 'checkout',
                'paction' => 'payment',
                'conn'=>'SSL',
                'params' => 'error_vrpayecommerce_payment='.$return_message
            )
        );
        $xtLink->_redirect($link);
    }
}
