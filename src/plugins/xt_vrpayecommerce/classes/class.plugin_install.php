<?php
/**
 * VR pay eCommerce - XTC4
 *
 * @copyright Copyright (c) 2015 VR pay eCommerce
 * @author    VR pay eCommerce <www.vr-epay.info>
 * @package   Vrpayecommerce/Classes
 * @located   at   classes/
 */

defined('_VALID_CALL') or die('Direct Access is not allowed.');

/**
 * Process for Plugin Install
 */
class plugin_install extends plugin
{
    /**
*
     *
 * @var array vrpay payment status
*/
    public $vrpay_status = array(
        'IR' => array(
            'en' => 'In Review',
            'de' => 'Wird überprüft',
            'key' => 'XT_VRPAYECOMMERCE_STATUS_IR',
        ),
        'PA' => array(
            'en' => 'Pre-Authorization of Payment',
            'de' => 'Pre-Authorisierung der Zahlung',
            'key' => 'XT_VRPAYECOMMERCE_STATUS_PA',
        ),
        'RF' => array(
            'en' => 'Refund',
            'de' => 'Gutschrift',
            'key' => 'XT_VRPAYECOMMERCE_STATUS_RF',
        ),
    );

    public function __construct()
    {
    }

    /**
     * install plugin
     *
     * @param  string $payment_id
     * @return boolean
     */
    public function installPlugin($payment_id)
    {
        global $db, $store_handler, $language;

        for ($i = 24; $i < 32; $i++) {
            $db->Execute(
                "INSERT INTO ".TABLE_PAYMENT_COST." (
                    `payment_id`,
                    `payment_geo_zone`,
                    `payment_country_code`,
                    `payment_type_value_from`,
                    `payment_type_value_to`, 
                    `payment_price`,
                    `payment_allowed`
                ) VALUES(".$payment_id.", ".$i.", '', 0, 10000.00, 0, 1);"
            );
        }
        $this->installVrpayOrderTable();
        $this->installVrpayRecurringTable();
        $this->setupOrderStatus();

        return true;
    }

    /**
     * install vrpayecommerce order table
     *
     * @return void
     */
    public function installVrpayOrderTable()
    {
        global $db;

        $db->Execute(
            "CREATE TABLE IF NOT EXISTS ".DB_PREFIX."_payment_vrpayecommerce_orders (
                id INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
                orders_id INT(11) NOT NULL,
                payment_method VARCHAR(50) NOT NULL,
                unique_id VARCHAR(32) NOT NULL,
                amount DECIMAL(15,4) NOT NULL,
                currency VARCHAR(3) NOT NULL,
                mandate_id VARCHAR(100) NOT NULL,
                mandate_date VARCHAR(10) NOT NULL,
                payment_type VARCHAR(2) NOT NULL
            ) ENGINE=MyISAM DEFAULT CHARSET=utf8;"
        );
    }

    /**
     * install vrpayecommerce recurring table
     *
     * @return void
     */
    public function installVrpayRecurringTable()
    {
        global $db;

        $db->Execute(
            "CREATE TABLE IF NOT EXISTS ".DB_PREFIX."_payment_vrpayecommerce_recurring (
                id INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
                cust_id INT(11) NOT NULL,
                payment_group VARCHAR(32) NOT NULL,
                brand VARCHAR(100) NOT NULL,
                holder VARCHAR(100) NOT NULL,
                email VARCHAR(100) NOT NULL,
                last4digits VARCHAR(4) NOT NULL,
                expiry_month VARCHAR(2) NOT NULL,
                expiry_year VARCHAR(4) NOT NULL,
                ref_id VARCHAR(32) NOT NULL,
                payment_default boolean NOT NULL default '0'
            ) ENGINE=MyISAM DEFAULT CHARSET=utf8;"
        );

        $rs = $db->Execute("SHOW columns FROM ".DB_PREFIX."_payment_vrpayecommerce_recurring LIKE 'server_mode'");
        if ($rs->RecordCount() < 1) {
            $db->Execute(
                "ALTER TABLE ".DB_PREFIX."_payment_vrpayecommerce_recurring 
                ADD server_mode VARCHAR(4) NOT NULL AFTER expiry_year, 
                ADD channel_id VARCHAR(32) NOT NULL AFTER server_mode"
            );
        }
    }

    /**
     * setup order status
     *
     * @return void
     */
    public function setupOrderStatus()
    {
        global $db;

        $status_id = $this->isStatusExist('Payment received');
        $db->Execute(
            "UPDATE ".TABLE_CONFIGURATION_PAYMENT." 
            SET config_value = $status_id 
            WHERE config_key = 'XT_VRPAYECOMMERCE_STATUS_ACCEPT'"
        );

        foreach ($this->vrpay_status as $key => $status) {
            if ($key == 'RF') {
                $this->installOrderStatus($status, 1, 1);
            } else {
                $this->installOrderStatus($status);
            }
        }
    }

    /**
     * validate status is exist or not
     *
     * @param  string $status_name
     * @return string|boolean
     */
    public function isStatusExist($status_name)
    {
        global $db;
        
        $status_id = $db->GetOne(
            "SELECT status_id FROM ".TABLE_SYSTEM_STATUS_DESCRIPTION." WHERE status_name = '".$status_name."'"
        );

        if ($status_id) {
            return $status_id;
        }

        return false;
    }

    /**
     * install vrpay order status
     *
     * @param  array $status
     * @param  int   $enable_download
     * @param  int   $calculate_statistic
     * @return string|boolean
     */
    public function installOrderStatus($status, $enable_download = 0, $calculate_statistic = 0)
    {
        global $db;

        $status_id = $this->isStatusExist($status['en']);
        if ($status_id == false) {
            $status_values = $this->getStatusValue($enable_download, $calculate_statistic);
            $db->Execute(
                "INSERT INTO ".TABLE_SYSTEM_STATUS." (status_class, status_values) 
                VALUES ('order_status', '".$status_values."')"
            );
            $status_id = $db->Insert_ID(TABLE_SYSTEM_STATUS, 'status_id');
            $db->Execute(
                "INSERT INTO ".TABLE_SYSTEM_STATUS_DESCRIPTION." (status_id, language_code, status_name) 
                VALUES ('".$status_id."', 'en', '".$status['en']."')"
            );
            $db->Execute(
                "INSERT INTO ".TABLE_SYSTEM_STATUS_DESCRIPTION." (status_id, language_code, status_name) 
                VALUES ('".$status_id."', 'de', '".$status['de']."')"
            );
        }

        $db->Execute(
            "UPDATE ".TABLE_CONFIGURATION_PAYMENT." 
            SET config_value = $status_id WHERE config_key = '".$status['key']."'"
        );
    }

    /**
     * get status value in serialize mode
     *
     * @param  int $enable_download
     * @param  int $calculate_statistic
     * @return string
     */
    public function getStatusValue($enable_download, $calculate_statistic)
    {
        $parameters['data']['enable_download'] = $enable_download;
        $parameters['data']['visible'] = '1';
        $parameters['data']['visible_admin'] = 1;
        $parameters['data']['calculate_statistic'] = $calculate_statistic;
        $parameters['data']['reduce_stock'] = 0;
        
        return serialize($parameters);
    }

    /**
     * uninstall plugin
     *
     * @return boolean
     */
    public function uninstallPlugin()
    {
        global $db;

        $db->Execute("DROP TABLE IF EXISTS ".DB_PREFIX."_payment_vrpayecommerce_orders");
        $db->Execute("DROP TABLE IF EXISTS ".DB_PREFIX."_payment_vrpayecommerce_recurring");

        return true;
    }
}
