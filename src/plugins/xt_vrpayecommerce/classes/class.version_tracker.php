<?php
/**
 * VR pay eCommerce - XTC4
 *
 * @copyright Copyright (c) 2015 VR pay eCommerce
 * @author    VR pay eCommerce <www.vr-epay.info>
 * @package   Vrpayecommerce/Classes
 * @located   at   classes/
 */

defined('_VALID_CALL') or die('Direct Access is not allowed.');

/**
 * Vrpayecommerce Version Tracker
 */
class version_tracker
{
    /**
     *
     *
     * @var string url for version tracker
     */
    private static $version_tracker_url = 'http://api.dbserver.payreto.eu/v1/tracker';

    /**
     * get version tracker url
     *
     * @return string
     */
    private static function getVersionTrackerUrl()
    {
        return self::$version_tracker_url;
    }

    /**
     * get version tracker parameter
     *
     * @param  array $version_data
     * @return string
     */
    private static function getVersionTrackerParameter($version_data)
    {
         $version_data['hash'] = md5($version_data['shop_version'].
            $version_data['plugin_version'].
            $version_data['client']);

        return http_build_query(array_filter($version_data), '', '&');
    }

    /**
     * sent data to version tracker
     *
     * @param  array $version_data
     * @return array|boolean
     */
    public static function sendVersionTracker($version_data)
    {
        $post_data = self::getVersionTrackerParameter($version_data);
        $url = self::getVersionTrackerUrl();
        return vrpayecommerce_payment_core::requestPaymentResponse($post_data, $url, $version_data['transaction_mode']);
    }
}
