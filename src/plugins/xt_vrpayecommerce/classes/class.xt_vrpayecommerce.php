<?php
/**
 * VR pay eCommerce - XTC4
 *
 * @copyright Copyright (c) 2015 VR pay eCommerce
 * @author    VR pay eCommerce <www.vr-epay.info>
 * @package   Vrpayecommerce/Classes
 * @located   at   classes/
 */

defined('_VALID_CALL') or die('Direct Access is not allowed.');
require_once _SRV_WEBROOT."plugins/xt_vrpayecommerce/classes/class.opp.php";

/**
 * VR pay eCommerce Payment Class
 */
class xt_vrpayecommerce
{
    /**
*
     *
 * @var string vrpayecommerce plugin version
*/
    public $version = '1.0.02';
    /**
*
     *
 * @var string client name
*/
    public $client = 'CardProcess';
    public $shop_system = 'XtCommerce 4';

    /**
*
     *
 * @var boolean external value
*/
    public $external = true;
    /**
*
     *
 * @var boolean subpayment
*/
    public $subpayments = false;
    /**
*
     *
 * @var boolean iframe for payment widget
*/
    public $iframe = true;
    /**
*
     *
 * @var boolean frame for payment action
*/
    public $pay_frame = true;
    /**
*
     *
 * @var boolean form for payment post
*/
    public $post_form = false;
    /**
*
     *
 * @var boolean plugin data
*/
    public $data = array();
    /**
*
     *
 * @var boolean url for payment iframe
*/
    public $IFRAME_URL;
    /**
*
     *
 * @var boolean payment method
*/
    public $payment_method;
    /**
*
     *
 * @var boolean payment testmode
*/
    public $test_mode = 'EXTERNAL';

    public function __construct()
    {
        global $xtLink;
        $this->IFRAME_URL  = $xtLink->_link(
            array('page'=>'callback', 'paction'=>'xt_vrpayecommerce','params' => 'doCheckout=1')
        );
        if (isset($_SESSION['selected_payment_sub'])) {
            $this->payment_method = $_SESSION['selected_payment_sub'];
        } else {
            $this->payment_method = '';
        }
        $this->user_id = $_SESSION['customer']->customers_id;
        $this->deleteKlarnaPaymentMethod();
        //$this->insertKlarnaPaymentMethod();
        $this->addLogVrpayecommerce('get user_id = ', $this->user_id);
        $this->addLogVrpayecommerce('get payment_method = ', $this->payment_method);
    }

    /**
     * delete payment method on database
     * @return void
     */
    public function deleteKlarnaPaymentMethod()
    {
        global $db;
        $db->Execute("
                DELETE FROM ".DB_PREFIX."_config_payment WHERE `config_key`='XT_VRPAYECOMMERCE_KLARNASLICEIT_ACTIVATE'
            ");
        $db->Execute("
                DELETE FROM ".DB_PREFIX."_config_payment WHERE `config_key`='XT_VRPAYECOMMERCE_KLARNASLICEIT_SORT'
            ");
        $db->Execute("
                DELETE FROM ".DB_PREFIX."_config_payment WHERE `config_key`='XT_VRPAYECOMMERCE_KLARNASLICEIT_PCLASS'
            ");
        $db->Execute("
                DELETE FROM ".DB_PREFIX."_config_payment WHERE `config_key`='XT_VRPAYECOMMERCE_KLARNASLICEIT_ENTITY_ID'
            ");
        $db->Execute("
                DELETE FROM ".DB_PREFIX."_config_payment WHERE `config_key`='XT_VRPAYECOMMERCE_KLARNASLICEIT_SERVER'
            ");
        $db->Execute("
                DELETE FROM ".DB_PREFIX."_config_payment WHERE `config_key`='XT_VRPAYECOMMERCE_KLARNAPAYLATER_ENTITY_ID'
            ");
        $db->Execute("
                DELETE FROM ".DB_PREFIX."_config_payment WHERE `config_key`='XT_VRPAYECOMMERCE_KLARNAPAYLATER_SORT'
            ");
        $db->Execute("
                DELETE FROM ".DB_PREFIX."_config_payment WHERE `config_key`='XT_VRPAYECOMMERCE_KLARNAPAYLATER_SERVER'
            ");
        $db->Execute("
                DELETE FROM ".DB_PREFIX."_config_payment WHERE `config_key`='XT_VRPAYECOMMERCE_KLARNAPAYLATER_ACTIVATE'
            ");
    }

    /**
     * insert payment method to database
     * @return void
     */
    public function insertKlarnaPaymentMethod()
    {
        global $db;
        $db->Execute("
                INSERT INTO ".DB_PREFIX."_config_payment(`id`,
                    `config_key`,
                    `config_value`,
                    `group_id`,
                    `sort_order`,
                    `last_modified`,
                    `date_added`,
                    `type`,
                    `url`,
                    `payment_id`,
                    `shop_id`
                ) VALUES (
                    569,
                    'XT_VRPAYECOMMERCE_KLARNASLICEIT_ACTIVATE','1',0,255,NULL,NULL,'status','',11,1)
            ");
        $db->Execute("
                INSERT INTO ".DB_PREFIX."_config_payment(`id`,
                    `config_key`,
                    `config_value`,
                    `group_id`,
                    `sort_order`,
                    `last_modified`,
                    `date_added`,
                    `type`,
                    `url`,
                    `payment_id`,
                    `shop_id`
                ) VALUES (
                    573,
                    'XT_VRPAYECOMMERCE_KLARNASLICEIT_SORT','10',0,275,NULL,NULL,'textfield','',11,1)
            ");
        $db->Execute("
                INSERT INTO ".DB_PREFIX."_config_payment(`id`,
                    `config_key`,
                    `config_value`,
                    `group_id`,
                    `sort_order`,
                    `last_modified`,
                    `date_added`,
                    `type`,
                    `url`,
                    `payment_id`,
                    `shop_id`
                ) VALUES (
                    572,
                    'XT_VRPAYECOMMERCE_KLARNASLICEIT_PCLASS','-1',0,270,NULL,NULL,'textfield','',11,1)
            ");
        $db->Execute("
                INSERT INTO ".DB_PREFIX."_config_payment(`id`,
                    `config_key`,
                    `config_value`,
                    `group_id`,
                    `sort_order`,
                    `last_modified`,
                    `date_added`,
                    `type`,
                    `url`,
                    `payment_id`,
                    `shop_id`
                ) VALUES (
                    571,
                    'XT_VRPAYECOMMERCE_KLARNASLICEIT_ENTITY_ID',
                    '8a8294174d0a8edd014d0acd380b0232',
                    0,
                    265,
                    NULL,
                    NULL,
                    'textfield',
                    '',
                    11,
                    1
                )
            ");
        $db->Execute("
                INSERT INTO ".DB_PREFIX."_config_payment(`id`,
                    `config_key`,
                    `config_value`,
                    `group_id`,
                    `sort_order`,
                    `last_modified`,
                    `date_added`,
                    `type`,
                    `url`,
                    `payment_id`,
                    `shop_id`
                ) VALUES (
                    570,
                    'XT_VRPAYECOMMERCE_KLARNASLICEIT_SERVER',
                    'TEST',
                    0,
                    260,
                    NULL,
                    NULL,
                    'dropdown',
                    'xt_vrpayecommerce:server',
                    11,
                    1
                )
            ");
        $db->Execute("
                INSERT INTO ".DB_PREFIX."_config_payment(`id`,
                    `config_key`,
                    `config_value`,
                    `group_id`,
                    `sort_order`,
                    `last_modified`,
                    `date_added`,
                    `type`,
                    `url`,
                    `payment_id`,
                    `shop_id`
                ) VALUES (
                    567,
                    'XT_VRPAYECOMMERCE_KLARNAPAYLATER_ENTITY_ID',
                    '8a8294174d0a8edd014d0acd380b0232',
                    0,
                    245,
                    NULL,
                    NULL,
                    'textfield',
                    '',
                    11,
                    1
                )
            ");
        $db->Execute("
                INSERT INTO ".DB_PREFIX."_config_payment(`id`,
                    `config_key`,
                    `config_value`,
                    `group_id`,
                    `sort_order`,
                    `last_modified`,
                    `date_added`,
                    `type`,
                    `url`,
                    `payment_id`,
                    `shop_id`
                ) VALUES (
                    568,
                    'XT_VRPAYECOMMERCE_KLARNAPAYLATER_SORT',
                    '9',
                    0,
                    250,
                    NULL,
                    NULL,
                    'textfield',
                    '',
                    11,
                    1
                )
            ");
        $db->Execute("
                INSERT INTO ".DB_PREFIX."_config_payment(`id`,
                    `config_key`,
                    `config_value`,
                    `group_id`,
                    `sort_order`,
                    `last_modified`,
                    `date_added`,
                    `type`,
                    `url`,
                    `payment_id`,
                    `shop_id`
                ) VALUES (
                    566,
                    'XT_VRPAYECOMMERCE_KLARNAPAYLATER_SERVER',
                    'TEST',
                    0,
                    240,
                    NULL,
                    NULL,
                    'dropdown',
                    'xt_vrpayecommerce:server',
                    11,
                    1
                )
            ");
        $db->Execute("
                INSERT INTO ".DB_PREFIX."_config_payment(`id`,
                    `config_key`,
                    `config_value`,
                    `group_id`,
                    `sort_order`,
                    `last_modified`,
                    `date_added`,
                    `type`,
                    `url`,
                    `payment_id`,
                    `shop_id`
                ) VALUES (
                    565,
                    'XT_VRPAYECOMMERCE_KLARNAPAYLATER_ACTIVATE',
                    '1',
                    0,
                    235,
                    NULL,
                    NULL,
                    'status',
                    '',
                    11,
                    1
                )
            ");
    }

    /**
     * get payment method config from database
     *
     * @param  string $key
     * @return string
     */
    public function getConfig($key)
    {
        return constant('XT_VRPAYECOMMERCE_'.strtoupper($this->payment_method).'_'.strtoupper($key));
    }

    /**
     * get payment general setting VR pay eCommerce plugin
     *
     * @param  string $key
     * @return string
     */
    public function getGeneralConfig($key)
    {
        return constant('XT_VRPAYECOMMERCE_GENERAL_'.strtoupper($key));
    }

    /**
     * get server mode
     *
     * @return string
     */
    public function getServerMode()
    {
        return $this->getConfig('SERVER');
    }

    /**
     * get payment credential
     *
     * @return array
     */
    public function getCredentials()
    {
        $credentials = array(
            'server_mode' => $this->getServerMode(),
            'channel_id'  => $this->getConfig('entity_id'),
            'login'       => $this->getGeneralConfig('user_id'),
            'password'    => $this->getGeneralConfig('password')
        );

        if ($this->isMultiChannel()) {
            $credentials['channel_id_moto'] = $this->getConfig('entity_id_moto');
        }

        return $credentials;
    }

    /**
     * check credit card recurring multichannel active or not
     *
     * @return boolean
     */
    public function isMultiChannel()
    {
        if ($this->payment_method == 'CCSAVED') {
            return $this->getConfig('multichannel');
        }
        return false;
    }

    /**
     * get amount for register payment account
     *
     * @return string
     */
    public function getRegisterAmount()
    {
        return $this->getConfig('amount');
    }

    /**
     * get payment type
     *
     * @return string
     */
    public function getPaymentTypeSelection()
    {
        return $this->getConfig('payment_type');
    }

    /**
     * get payment is partial or not
     *
     * @return boolean
     */
    public function getPaymentIsPartial()
    {
        return $this->getConfig('payment_is_partial');
    }

    /**
     * get minimum age for paydirekt method
     *
     * @return string
     */
    public function getMinimumAge()
    {
        return $this->getConfig('minimum_age');
    }

    /**
     * get payment type
     *
     * @return string
     */
    public function getPaymentType()
    {
        switch ($this->payment_method) {
            case 'CC':
            case 'CCSAVED':
            case 'DC':
            case 'DD':
            case 'DDSAVED':
            case 'PAYDIREKT':
                return $this->getPaymentTypeSelection();
                break;
            case 'KLARNAPAYLATER':
            case 'KLARNASLICEIT':
            case 'EASYCREDIT':
                return 'PA';
                break;
            default:
                return 'DB';
                break;
        }
    }

    /**
     * get payment brand
     *
     * @return string
     */
    public function getBrand()
    {
        switch ($this->payment_method) {
            case 'CC':
            case 'CCSAVED':
                return $this->getCardBrand();
                break;
            case 'DC':
                return 'VPAY MAESTRO DANKORT VISAELECTRON POSTEPAY';
                break;
            case 'DD':
            case 'DDSAVED':
                return 'DIRECTDEBIT_SEPA';
                break;
            case 'KLARNAPAYLATER':
                return 'KLARNA_INVOICE';
                break;
            case 'KLARNASLICEIT':
                return 'KLARNA_INSTALLMENTS';
                break;
            case 'PAYPALSAVED':
                return 'PAYPAL';
                break;
            case 'KLARNAOBT':
                return 'SOFORTUEBERWEISUNG';
                break;
            case 'EASYCREDIT':
                return 'RATENKAUF';
                break;
            default:
                return $this->payment_method;
                break;
        }
    }

    /**
     * get payment brand for Credit Card
     *
     * @return string
     */
    public function getCardBrand()
    {
        $brand = '';
        if ($this->getConfig('visa')) {
            $brand .= 'VISA ';
        }
        if ($this->getConfig('master')) {
            $brand .= 'MASTER ';
        }
        if ($this->getConfig('amex')) {
            $brand .= 'AMEX ';
        }
        if ($this->getConfig('diners')) {
            $brand .= 'DINERS ';
        }
        if ($this->getConfig('jcb')) {
            $brand .= 'JCB ';
        }

        return trim($brand);
    }

    /**
     * get testmode
     *
     * @return boolean|string
     */
    public function getTestMode()
    {
        if ($this->getServerMode() == "LIVE") {
            return false;
        }

        switch ($this->payment_method) {
            case 'GIROPAY':
                return 'INTERNAL';
            default:
                return $this->test_mode;
        }
    }

    /**
     * check payment recurring is active or not
     *
     * @return boolean
     */
    public function isRecurringActive()
    {
        return $this->getGeneralConfig('recurring');
    }

    /**
     * check version tracker is active or not
     *
     * @return boolean
     */
    public function isVersionTrackerActive()
    {
        return $this->getGeneralConfig('version_tracker');
    }

    /**
     * check payment method is recurring or not
     *
     * @return boolean
     */
    public function isRecurring()
    {
        switch ($this->payment_method) {
            case 'CCSAVED':
            case 'DDSAVED':
            case 'PAYPALSAVED':
                return true;
                break;
            default:
                return false;
                break;
        }
    }

    /**
     * get payment methods group for recurring payment
     *
     * @return string|boolean
     */
    public function getRecurringGroup()
    {
        switch ($this->payment_method) {
            case 'CCSAVED':
                return 'CC';
                break;
            case 'DDSAVED':
                return 'DD';
                break;
            case 'PAYPALSAVED':
                return 'VA';
                break;
        }
        return false;
    }

    /**
     * check payment method is redirect or not
     *
     * @return boolean
     */
    public function isRedirect()
    {
        switch ($this->payment_method) {
            case 'PAYPAL':
            case 'PAYDIREKT':
            case 'EASYCREDIT':
                return true;
                break;
            default:
                return false;
                break;
        }
    }

    /**
     * get customer IP
     *
     * @return string
     */
    public function getCustomerIp()
    {
        if ($_SERVER['REMOTE_ADDR'] == '::1') {
            return '127.0.0.1';
        }

        return $_SERVER['REMOTE_ADDR'];
    }

    /**
     * get administrator email
     *
     * @return string
     */
    public function getAdminEmail()
    {
        global $db;

        $rs = $db->Execute(
            "SELECT email from ".TABLE_ADMIN_ACL_AREA_USER." where group_id=? and status=?",
            array('1', '1')
        );
        return $rs->fields['email'];
    }

    /**
     * get merchant email
     *
     * @return string
     */
    public function getMerchantEmail()
    {
        $merchant_email = $this->getGeneralConfig('merchant_email');
        if ($merchant_email) {
            return $merchant_email;
        }

        return $this->getAdminEmail();
    }

    /**
     * get version tracker parameters
     *
     * @return  array
     */
    public function getVersionData()
    {
        return array_merge(
            $this->getGeneralVersionData(),
            $this->getCreditCardVersionData()
        );
    }

    /**
     * get general version tracker parameters
     *
     * @return array
     */
    protected function getGeneralVersionData()
    {
        $version_data = array();

        $version_data['transaction_mode'] = $this->getServerMode();
        $version_data['ip_address'] = $_SERVER['SERVER_ADDR'];
        $version_data['shop_version'] = constant('_SYSTEM_VERSION');
        $version_data['plugin_version'] = $this->version;
        $version_data['client'] = $this->client;
        $version_data['email'] = $this->getMerchantEmail();
        $version_data['merchant_id'] = $this->getGeneralConfig('merchant_no');
        $version_data['shop_system'] = $this->shop_system;
        $version_data['shop_url'] = $this->getGeneralConfig('shop_url');

        return $version_data;
    }

    /**
     * get credit card version tracker parameters
     *
     * @return array
     */
    protected function getCreditCardVersionData()
    {
        $version_data = array();

        if ($this->payment_method == 'CC' || $this->payment_method == 'CCSAVED') {
            $version_data['merchant_location'] = $this->getGeneralConfig('merchant_location');
        }

        return $version_data;
    }

    /**
     * get account type for recurring payment
     *
     * @return string|boolean
     */
    public function getAccountType()
    {
        switch ($this->payment_method) {
            case 'CCSAVED':
                return 'card';
                break;
            case 'DDSAVED':
                return 'bankAccount';
                break;
            case 'PAYPALSAVED':
                return 'virtualAccount';
                break;
        }
        return false;
    }

    /**
     * get account detail from payment response
     *
     * @param  array $payment_response
     * @return array
     */
    public function getAccount($payment_response)
    {
        $account = $payment_response[$this->getAccountType()];
        switch ($this->payment_method) {
            case 'DDSAVED':
                $account['last4Digits'] = substr($account['iban'], -4);
                break;
            case 'PAYPALSAVED':
                $account['email'] = $account['accountId'];
                break;
        }
        return $account;
    }

    /**
     * check default account payment
     *
     * @return boolean
     */
    public function isDefaultAccount()
    {
        global $db;

        $credentials = $this->getCredentials();
        $rs = $db->Execute(
            "SELECT * from ".DB_PREFIX."_payment_vrpayecommerce_recurring
			where cust_id=? and server_mode=? and channel_id=? and payment_group=? and payment_default=?",
            array(
                $this->user_id,
                $credentials['server_mode'],
                $credentials['channel_id'],
                $this->getRecurringGroup(),
                1
            )
        );

        if ($rs->RecordCount() > 0) {
            return false;
        }

        return true;
    }

    /**
     * Get payment response after doing payment
     *
     * @return  void
     */
    public function getEasycreditPaymentResponse()
    {
        global $order;

        $checkout_id = $_REQUEST['id'];

        $transaction_data = $this->getCredentials();

        $payment_result = vrpayecommerce_payment_core::getPaymentServerToServerStatus(
            $checkout_id,
            $transaction_data
        );

        if (!$payment_result['is_valid']) {
            vrpayecommerce_payment_core::redirectError($payment_result['response']);
        } else {
            $code_result = $payment_result['response']["result"]["code"];
            $transaction_result = vrpayecommerce_payment_core::getTransactionResult($code_result);

            if ($transaction_result == "ACK") {
                $order_status = constant('XT_VRPAYECOMMERCE_STATUS_PA');
                $orders_id = $_SESSION['last_order_id'];
                $order = new order($orders_id, $this->user_id);
                $order->_updateOrderStatus($order_status, '', 'true', 'true', 'IPN', 'VR pay eCommerce');
                return $payment_result['response'];
            } elseif ($transaction_result == "NOK") {
                $error_identifier = vrpayecommerce_payment_core::getErrorIdentifier($code_result);
                vrpayecommerce_payment_core::redirectError($error_identifier);
            } else {
                vrpayecommerce_payment_core::redirectError('ERROR_UNKNOWN');
            }
        }
    }

    /**
     * check account payment is registered or not
     *
     * @param  string $registration_id
     * @return boolean
     */
    public function isRegistered($registration_id)
    {
        global $db;

        $rs = $db->Execute(
            "SELECT ref_id from ".DB_PREFIX."_payment_vrpayecommerce_recurring
			where ref_id=? and cust_id=? and payment_group=?",
            array($registration_id,$this->user_id,$this->getRecurringGroup())
        );

        if ($rs->RecordCount() > 0) {
            return true;
        }

        return false;
    }

    /**
     * get reference id
     *
     * @param  int $id
     * @return string
     */
    public function getReferenceId($id)
    {
        global $db;

        $rs = $db->Execute(
            "SELECT ref_id from ".DB_PREFIX."_payment_vrpayecommerce_recurring
			where id=? and cust_id=? and payment_group=?",
            array($id,$this->user_id,$this->getRecurringGroup())
        );
        return $rs->fields['ref_id'];
    }

    /**
     * get registered account payment
     *
     * @return array|boolean
     */
    public function getRegistrations()
    {
        global $db;

        $credentials = $this->getCredentials();
        $rs = $db->Execute(
            "SELECT * from ".DB_PREFIX."_payment_vrpayecommerce_recurring
			where cust_id=? and server_mode=? and channel_id=? and payment_group=?",
            array($this->user_id, $credentials['server_mode'], $credentials['channel_id'], $this->getRecurringGroup())
        );

        if ($rs->RecordCount() > 0) {
            return $rs;
        }

        return false;
    }

    /**
     * update registratiion default
     *
     * @param  int $id
     * @return void
     */
    public function updateDefaultRegistration($id)
    {
        global $db;

        $db->Execute(
            "UPDATE ".DB_PREFIX."_payment_vrpayecommerce_recurring SET payment_default = ?
			WHERE payment_default = ? AND payment_group = ?",
            array('0','1',$this->getRecurringGroup())
        );

        $db->Execute(
            "UPDATE ".DB_PREFIX."_payment_vrpayecommerce_recurring SET payment_default = ?
			WHERE id = ? AND payment_group = ?",
            array('1',$id,$this->getRecurringGroup())
        );
    }

    /**
     * insert registration
     *
     * @param  string $registration_id
     * @param  array  $registration_response
     * @return void
     */
    public function insertRegistration($registration_id, $registration_response)
    {
        global $db;
        $this->addLogVrpayecommerce('start register payment account ');

        $is_registered = $this->isRegistered($registration_id);
        $this->addLogVrpayecommerce('is_registered = ', $is_registered);

        if (!$is_registered) {
            $credentials = $this->getCredentials();
            $this->addLogVrpayecommerce('get credentials parameter = ', $credentials);

            $default = $this->isDefaultAccount();
            $this->addLogVrpayecommerce('isDefaultAccount = ', $default);

            $account = $this->getAccount($registration_response);
            $this->addLogVrpayecommerce('get payment account = ', $account);

            if (!isset($account['email'])) {
                $account['email'] = '';
            }
            if (!isset($account['expiryMonth'])) {
                $account['expiryMonth'] = '';
            }
            if (!isset($account['expiryYear'])) {
                $account['expiryYear'] = '';
            }
            if (!isset($account['last4Digits'])) {
                $account['last4Digits'] = '';
            }
            $db->Execute(
                "INSERT INTO ".DB_PREFIX."_payment_vrpayecommerce_recurring (
					cust_id, payment_group, brand, holder, email, last4digits, expiry_month,
					expiry_year, server_mode, channel_id, ref_id, payment_default
				)
				VALUES (
					'".$this->user_id."', '".$this->getRecurringGroup()."', '"
                    .$registration_response['paymentBrand']."',
					'".$account['holder']."', '".$account['email']."', '".$account['last4Digits']."',
					'".$account['expiryMonth']."', '".$account['expiryYear']."', '".$credentials['server_mode']."',
					'".$credentials['channel_id']."', '".$registration_id."', '".$default."'
				)"
            );
        }
    }

    /**
     * update registration
     *
     * @param  string $recurring_id
     * @param  string $registration_id
     * @param  array  $registration_response
     * @return void
     */
    public function updateRegistration($recurring_id, $registration_id, $registration_response)
    {
        global $db;
        $this->addLogVrpayecommerce('start update payment account ');

        $credentials = $this->getCredentials();
        $this->addLogVrpayecommerce('get credential parameter = ', $credentials);

        $account = $this->getAccount($registration_response);
        $this->addLogVrpayecommerce('get previous account = ', $account);

        if (!isset($account['email'])) {
            $account['email'] = '';
        }
        if (!isset($account['expiryMonth'])) {
            $account['expiryMonth'] = '';
        }
        if (!isset($account['expiryYear'])) {
            $account['expiryYear'] = '';
        }
        if (!isset($account['last4Digits'])) {
            $account['last4Digits'] = '';
        }
        $db->Execute(
            "UPDATE ".DB_PREFIX."_payment_vrpayecommerce_recurring
			SET cust_id=?, payment_group=?, brand=?, holder=?, email=?, last4digits=?, expiry_month=?,
				expiry_year=?, server_mode=?, channel_id=?, ref_id=? WHERE id=?",
            array(
                $this->user_id, $this->getRecurringGroup(), $registration_response['paymentBrand'],
                $account['holder'], $account['email'], $account['last4Digits'], $account['expiryMonth'],
                $account['expiryYear'], $credentials['server_mode'], $credentials['channel_id'],
                $registration_id, $recurring_id
            )
        );
    }

    /**
     * save registration
     *
     * @param  string $recurring_id
     * @param  string $registration_id
     * @param  array  $registration_response
     * @return void
     */
    public function saveRegistration($recurring_id, $registration_id, $registration_response)
    {
        if ($recurring_id) {
            $this->updateRegistration($recurring_id, $registration_id, $registration_response);
        } else {
            $this->insertRegistration($registration_id, $registration_response);
        }
    }

    /**
     * delete recurring payment account
     *
     * @param  int $id
     * @return void
     */
    public function deleteRecurring($id)
    {
        global $db;

        $db->Execute(
            "DELETE FROM ".DB_PREFIX."_payment_vrpayecommerce_recurring where id=? and cust_id=? and payment_group=?",
            array($id,$this->user_id,$this->getRecurringGroup())
        );
    }

    /**
     * get klarna parameters for checkout payment
     *
     * @return array
     */
    public function getKlarnaParameters()
    {
        $klarna_parameters = array();
        if ($this->payment_method == 'KLARNAPAYLATER' || $this->payment_method == 'KLARNASLICEIT') {
            $klarna_parameters['customer']['sex'] = $this->getCustomerSex();
            $klarna_parameters['customer']['birthdate'] = $this->getCustomerDob();
            $klarna_parameters['customer']['phone'] =
                $_SESSION['customer']->customer_default_address['customers_phone'];
            $klarna_parameters['customer']['mobile'] =
                $_SESSION['customer']->customer_default_address['customers_mobile_phone'];
            $klarna_parameters['customParameters']['KLARNA_CART_ITEM1_FLAGS'] = 32;
            $klarna_parameters['cartItems'] = $this->getCartItems();
        }
        if ($this->payment_method == 'KLARNASLICEIT') {
            $klarna_parameters['customParameters']['KLARNA_PCLASS_FLAG'] = $this->getConfig('pclass');
        }
        return $klarna_parameters;
    }

    /**
     * get paydirekt parameters for checkout payment
     *
     * @return array
     */
    public function getPaydirektParameters()
    {
        $paydirekt_parameters = array();
        if ($this->payment_method == 'PAYDIREKT') {
            $paydirekt_parameters['customParameters']['PAYDIREKT_minimumAge'] = $this->getMinimumAge();
            $paydirekt_parameters['customParameters']['PAYDIREKT_payment.isPartial'] = $this->getPaymentIsPartial();
        }
        return $paydirekt_parameters;
    }

    /**
     * Get the Easycredit parameters
     *
     * @param array $order_info
     * @return  array
     */
    protected function getEasycreditParameters()
    {
        global $order, $xtLink;
        $easycredit_parameters = array();

        if ($this->payment_method == 'EASYCREDIT') {
            $easycredit_parameters['customer']['sex'] = $this->getCustomerSex();
            $easycredit_parameters['customer']['birthdate'] = $this->getCustomerDob();
            $easycredit_parameters['customer']['phone'] =
                $_SESSION['customer']->customer_default_address['customers_phone'];
            $easycredit_parameters['cartItems'] = $this->getCartItems();
            $easycredit_parameters['customParameters']['RISK_ANZAHLBESTELLUNGEN'] = $this->getCustomerTotalOrders();
            $easycredit_parameters['customParameters']['RISK_BESTELLUNGERFOLGTUEBERLOGIN'] = $this->isCustomerLogin();
            $easycredit_parameters['customParameters']['RISK_KUNDENSTATUS'] = $this->getRiskKundenStatus();
            $easycredit_parameters['customParameters']['RISK_KUNDESEIT'] = $this->getCustomerCreatedDate();
            $easycredit_parameters['paymentBrand'] = $this->getBrand();
            $easycredit_parameters['shipping']['city'] =
                $_SESSION['customer']->customer_shipping_address['customers_city'];
            $easycredit_parameters['shipping']['country'] =
                $_SESSION['customer']->customer_shipping_address['customers_country_code'];
            $easycredit_parameters['shipping']['postcode'] =
                $_SESSION['customer']->customer_shipping_address['customers_postcode'];
            $easycredit_parameters['shipping']['street1'] =
                $_SESSION['customer']->customer_shipping_address['customers_street_address'];

            $easycredit_parameters['shopperResultUrl'] =
                $xtLink->_link(
                    array(
                            'page'=>'checkout',
                            'paction'=>'order_confirmation',
                            'conn'=>'SSL'
                    )
                );
        }
        return $easycredit_parameters;
    }

    /**
     * Get customer total order
     *
     * @return int
     */
    public function getCustomerTotalOrders()
    {

        global $db;
        $customer_id = $_SESSION['customer']->customer_info['customers_id'];

        if ($this->isCustomerLogin() == 'true') {
                $query = $db->Execute(
                    "SELECT COUNT(*) as total FROM ".DB_PREFIX."_orders
					WHERE customers_id=?",
                    array(
                        $customer_id
                    )
                );

            if ($query->fields['total'] != 0) {
                return $query->fields['total'];
            }
        }
        return 0;
    }

    /**
     * Check customer login status and return as string
     *
     * @return string
     */
    protected function isCustomerLogin()
    {
        if (isset($_SESSION['customer']->customers_status)) {
            $customers_status = $_SESSION['customer']->customers_status;
        }

        if ($customers_status == 1) {
            return 'false';
        }

        return 'true';
    }

    /**
     * Get risk kunden status
     *
     * @return string
     */
    protected function getRiskKundenStatus()
    {
        if ($this->getCustomerTotalOrders() > 0) {
            return 'BESTANDSKUNDE';
        }
        return 'NEUKUNDE';
    }

    /**
     * Get customer created date
     *
     * @return string|boolean
     */
    protected function getCustomerCreatedDate()
    {
        global $db;
        $customer_id = $_SESSION['customer']->customer_info['customers_id'];

        if ($this->isCustomerLogin() == 'true') {
            $query = $db->Execute(
                "SELECT * FROM ".DB_PREFIX."_customers
					WHERE customers_id=?",
                array(
                        $customer_id
                    )
            );

            if (isset($query->fields['date_added'])) {
                return date("Y-m-d", strtotime($query->fields['date_added']));
            }
        }
        return date('Y-m-d');
    }

    /**
     * get registration parameters for checkout payment
     *
     * @return array
     */
    public function getRegistrationParameters()
    {
        $registration_parameters = array();
        if ($this->isRecurring()) {
            $registration_parameters['payment_recurring'] = 'INITIAL';
            $registration_parameters['payment_registration'] = 'true';
            $registrations = $this->getRegistrations();

            if ($registrations) {
                foreach ($registrations as $key => $value) {
                    $registration_parameters['registrations'][$key] = $value['ref_id'];
                }
            }
        }
        return $registration_parameters;
    }

    /**
     * get payment transaction parameters
     *
     * @return array
     */
    public function getTransactionParameters()
    {
        global $order, $currency;

        $transaction_parameters = $this->getCredentials();
        $transaction_parameters['customer']['email'] =
            $_SESSION['customer']->customer_info['customers_email_address'];
        $transaction_parameters['customer']['first_name'] =
            $_SESSION['customer']->customer_payment_address['customers_firstname'];
        $transaction_parameters['customer']['last_name'] =
            $_SESSION['customer']->customer_payment_address['customers_lastname'];
        $transaction_parameters['billing']['street'] =
            $_SESSION['customer']->customer_payment_address['customers_street_address'];
        $transaction_parameters['billing']['city'] =
            $_SESSION['customer']->customer_payment_address['customers_city'];
        $transaction_parameters['billing']['zip'] =
            $_SESSION['customer']->customer_payment_address['customers_postcode'];
        $transaction_parameters['billing']['country_code'] =
            $_SESSION['customer']->customer_payment_address['customers_country_code'];

        if (isset($_SESSION['cart']->total['plain'])) {
            $transaction_parameters['amount'] = $_SESSION['cart']->total['plain'];
        }

        $transaction_parameters['currency'] =  $currency->code;
        $transaction_parameters['customer_ip'] = $this->getCustomerIp();

        if ($this->payment_method == 'CCSAVED') {
            $transaction_parameters['3D']['amount'] = $transaction_parameters['amount'];
            $transaction_parameters['3D']['currency'] = $transaction_parameters['currency'];
        }
        $transaction_parameters['test_mode'] = $this->getTestMode();
        $transaction_parameters['payment_type'] = $this->getPaymentType();
        if ($this->payment_method == 'PAYPALSAVED') {
            unset($transaction_parameters['payment_type']);
        }
        $_SESSION['vrpayecommerce']['transaction_id'] = date('YmdHis') . $order->oID;
        $transaction_parameters['transaction_id'] = $_SESSION['vrpayecommerce']['transaction_id'];

        $transaction_parameters = array_merge_recursive(
            $transaction_parameters,
            $this->getKlarnaParameters(),
            $this->getPaydirektParameters(),
            $this->getEasycreditParameters(),
            $this->getRegistrationParameters()
        );

        $this->addLogVrpayecommerce('get transaction_parameters = ', $transaction_parameters);

        return $transaction_parameters;
    }

    /**
     * get cart item parameters for checkout payment
     *
     * @return array
     */
    public function getCartItems()
    {
        if (isset($_SESSION['cart']->show_content)) {
            $cart = $_SESSION['cart']->show_content;
        }
        $cart_items = array();
        $i = 0;

        foreach ($cart as $key => $value) {
            $cart_discount = '';
            if (isset($value['_cart_discount'])) {
                $cart_discount = (float) $value['_cart_discount'];
            }
            $cart_items[$i]['merchant_item_id'] = $value['products_id'];
            $cart_items[$i]['discount'] = $cart_discount;
            $cart_items[$i]['quantity'] = (int) $value['products_quantity'];
            $cart_items[$i]['name'] = $value['products_name'];
            $cart_items[$i]['price'] = $value['products_price']['plain'];
            $cart_items[$i]['tax'] = $value['products_tax_rate'];
            $i = $i+1;
        }

        $this->addLogVrpayecommerce('get cart items  = ', $cart_items);
        return $cart_items;
    }

    /**
     * get checkout result array & widget url for payment
     *
     * @param  string $payment_widget_url
     * @return string $payment_widget_url
     * @return array $checkout_result
     */
    public function getCheckout()
    {
        $transaction_parameters = $this->getTransactionParameters();
        $_SESSION['vrpayecommerce']['transaction_data'] = $transaction_parameters;
        $checkout_result = vrpayecommerce_payment_core::getCheckoutResult($transaction_parameters);
        $this->addLogVrpayecommerce('get checkout_result  = ', $checkout_result);
        $checkout_result['payment_widget_url'] = vrpayecommerce_payment_core::getPaymentWidgetUrl(
            $transaction_parameters['server_mode'],
            $checkout_result['response']['id']
        );

        return $checkout_result;
    }

    /**
     * get recurring checkout result & widget url for payment
     *
     * @param  string $payment_widget_url
     * @param  string|boolean $id
     * @return string $payment_widget_url
     * @return array $checkout_result
     */
    public function getCheckoutRecurring($id = false)
    {
        global $currency;

        $transaction_parameters = $this->getCredentials();
        $transaction_parameters['customer']['email'] =
            $_SESSION['customer']->customer_info['customers_email_address'];
        $transaction_parameters['customer']['first_name'] =
            $_SESSION['customer']->customer_payment_address['customers_firstname'];
        $transaction_parameters['customer']['last_name'] =
            $_SESSION['customer']->customer_payment_address['customers_lastname'];
        $transaction_parameters['billing']['street'] =
            $_SESSION['customer']->customer_payment_address['customers_street_address'];
        $transaction_parameters['billing']['city'] =
            $_SESSION['customer']->customer_payment_address['customers_city'];
        $transaction_parameters['billing']['zip'] =
            $_SESSION['customer']->customer_payment_address['customers_postcode'];
        $transaction_parameters['billing']['country_code'] =
            $_SESSION['customer']->customer_payment_address['customers_country_code'];

        $transaction_parameters['amount'] = $this->getRegisterAmount();
        $transaction_parameters['currency'] =  $currency->code;
        $transaction_parameters['customer_ip'] = $this->getCustomerIp();

        if ($this->payment_method == 'CCSAVED') {
            $transaction_parameters['3D']['amount'] = $transaction_parameters['amount'];
            $transaction_parameters['3D']['currency'] = $transaction_parameters['currency'];
        }
        if ($this->getRecurringGroup() != 'VA') {
            $transaction_parameters['payment_type'] = $this->getPaymentType();
        }
        $transaction_parameters['test_mode'] = $this->getTestMode();
        $transaction_parameters['payment_recurring'] = 'INITIAL';
        $transaction_parameters['payment_registration'] = 'true';

        if ($id) { // change payment
            $transaction_parameters['transaction_id'] = $this->getReferenceId($id);
        } else {
            $transaction_parameters['transaction_id'] = $this->user_id;
        }
        $this->addLogVrpayecommerce('get transaction_parameters = ', $transaction_parameters);

        $checkout_result = vrpayecommerce_payment_core::getCheckoutResult($transaction_parameters);

        $checkout_result['payment_widget_url'] = vrpayecommerce_payment_core::getPaymentWidgetUrl(
            $transaction_parameters['server_mode'],
            $checkout_result['response']['id']
        );

        if (!$this->isPaymentWidgetValid($checkout_result['payment_widget_url'])) {
            vrpayecommerce_payment_core::redirectError('ERROR_GENERAL_REDIRECT');
        }

        return $checkout_result;
    }


    /**
    * return true if doesn't find error in payment widget content
    *
    * @param $paymentWidgetUrl string
    *
    * @return array
    */
    public function isPaymentWidgetValid($paymentWidgetUrl)
    {
        $paymentWidgetContent = vrpayecommerce_payment_core::getPaymentWidgetContent(
            $paymentWidgetUrl,
            $this->getServerMode()
        );

        if ($paymentWidgetContent['is_valid']) {
            if (strpos($paymentWidgetContent['response'], 'errorDetail') !== false) {
                return false;
            }
            return true;
        } else {
            return false;
        }
    }

    /**
     * Get parameter request before checkout payment page
     *
     * @return string
     */
    public function getServerToServerRequest()
    {
        $transaction_parameters = array();

        $transaction_parameters = $this->getTransactionParameters();

        $_SESSION['vrpayecommerce']['transactionData'] = $transaction_parameters;

        $server_to_server_response = vrpayecommerce_payment_core::getServerToServerResponse($transaction_parameters);
        $this->addLogVrpayecommerce('get server_to_server_response = ', $transaction_parameters);

        return $server_to_server_response;
    }

    /**
    * is valid amount and currency
    *
    * @return boolean
    */
    public function isAmountAllowed()
    {
        global $currency;
        $amount_currency = $currency->code;
        if (isset($_SESSION['cart']->total['plain'])) {
            $total = $_SESSION['cart']->total['plain'];
        }

        if ($total > 200 && $total < 5000 && $amount_currency == 'EUR') {
            return true;
        }
        return false;
    }

    /**
    * is valid gender
    *
    * @return boolean
    */
    public function isGenderNotEmpty()
    {
        if (isset($_SESSION['customer']->customer_default_address['customers_gender'])) {
            $gender = $_SESSION['customer']->customer_default_address['customers_gender'];
        }

        if ($gender) {
            return true;
        }
        return false;
    }

    /**
    * is valid date of birth
    *
    * @return boolean
    */
    public function isDateOfBirthValid()
    {
        if (isset($_SESSION['customer']->customer_default_address['customers_dob'])) {
            $dob = $_SESSION['customer']->customer_default_address['customers_dob'];
        }
        $customer_date_of_birth = explode(".", $dob);
        if (!isset($customer_date_of_birth[0])) {
            return false;
        }

        if (!isset($customer_date_of_birth[1])) {
            return false;
        }

        if (!isset($customer_date_of_birth[2])) {
            return false;
        }

        $day = (int)$customer_date_of_birth[0];
        $month = (int)$customer_date_of_birth[1];
        $year = (int)$customer_date_of_birth[2];

        if ($year < 1900) {
            return false;
        }

        if ($month < 1 || $month > 12) {
            return false;
        }

        if ($day < 1 || $day > 31) {
            return false;
        }

        $valid = checkdate($month, $day, $year);

        if (!$valid) {
            return false;
        }

        return true;
    }

    /**
    * is valid date of birth
    *
    * @return boolean
    */
    public function isDateOfBirthLowerThanToday()
    {
        if (isset($_SESSION['customer']->customer_default_address['customers_dob'])) {
            $dob = $_SESSION['customer']->customer_default_address['customers_dob'];
        }

        $dob = strtotime($dob);
        $today = strtotime(date('Y-m-d'));

        if ($dob < $today) {
            return true;
        }
        return false;
    }

    /**
    * is valid address
    *
    * @return boolean
    */
    public function isBillingEqualShipping()
    {
        $address = $_SESSION['customer']->customer_payment_address;
        $shipping = $_SESSION['customer']->customer_shipping_address;

        if ($address == $shipping) {
            return true;
        }
        return false;
    }

    /**
     * get customer sex
     *
     * @return string
     */
    public function getCustomerSex()
    {
        $gender = $_SESSION['customer']->customer_default_address['customers_gender'];
        switch ($gender) {
            case 'm':
                return 'M';
                break;
            case 'f':
                return 'F';
                break;
            default:
                return '';
                break;
        }
    }

    /**
     * get shop language
     *
     * @return string
     */
    public function getLang()
    {
        global $language;

        $langs = $language->environment_language;
        switch ($langs) {
            case 'de':
                $lang = $langs;
                break;
            default:
                $lang='en';
        }
        return $lang;
    }

    /**
     * get customer date of birth
     *
     * @return string
     */
    public function getCustomerDob()
    {
        $date = $_SESSION['customer']->customer_default_address['customers_dob'];
        $dob = date("Y-m-d", strtotime($date));

        return $dob;
    }

    /**
     * redirect error when payment is error
     *
     * @return string $error_message
     * @return void
     */
    public function redirectCustomerError($error_message)
    {
        global $xtLink;

        $link = $xtLink->_link(
            array(
                'page' => 'customer',
                'paction' => 'payment_information',
                'conn'=>'SSL',
                'params' => 'error='.$error_message
            )
        );
        $xtLink->_redirect($link);
    }

    /**
     * redirect payment
     *
     * @return void
     */
    public function pspRedirect()
    {
        global $xtLink, $filter, $order, $db;
        $checkout_result = $this->getCheckout();

        if ($checkout_result['is_valid']) {
            if (!$this->isPaymentWidgetValid($checkout_result['payment_widget_url'])) {
                    vrpayecommerce_payment_core::redirectError('ERROR_GENERAL_REDIRECT');
            }

            if ($this->isRedirect() && $this->payment_method == "EASYCREDIT") {
                $this->addLogVrpayecommerce('server to server process');
                $request = $this->getServerToServerRequest();
                $transaction_result = vrpayecommerce_payment_core::getTransactionResult(
                    $request['response']['result']['code']
                );

                if ($transaction_result == 'NOK') {
                    vrpayecommerce_payment_core::redirectError('ERROR_GENERAL_REDIRECT');
                }
                $this->IFRAME_URL = $request['response']['redirect']['url'];
            } else {
                $this->IFRAME_URL = $xtLink->_link(
                    array(
                    'page'=>'callback',
                    'paction'=>'xt_vrpayecommerce',
                    'conn'=>'SSL',
                    'params' => 'do_checkout='.$this->payment_method.
                    '&payment_widget_url='.
                    urlencode($checkout_result['payment_widget_url'])
                    )
                );
            }

            if ($this->isRedirect()) {
                return $this->IFRAME_URL;
            } else {
                return  $xtLink->_link(
                    array(
                        'page'=>'checkout',
                        'paction'=>'pay_frame',
                        'conn'=>'SSL'
                    )
                );
            }
        } else {
            vrpayecommerce_payment_core::redirectError($checkout_result['response']);
        }
    }

    /**
     * add logging message
     *
     * @param string $message
     * @param mix $value
     *
     * @return void
     */
    public function addLogVrpayecommerce($message, $value = '')
    {
        error_log(
            $message.' '.print_r($value, true)."\n",
            3,
            _SRV_WEBROOT."xtLogs/vrpayecommerce-".date('d-m-Y').'.log'
        );
    }

        /**
     * Get merchant location from backend configuration value
     *
     * @return array
     */
    public function getMerchantLocation()
    {
        return $this->getGeneralConfig('merchant_location');
    }

    /**
     * return true if current payment method is using credit card recurring and credit card non recurring
     *
     * @return boolean
     */
    public function isCcPaymentMethod($payment_method)
    {
        if ($payment_method == 'CC' || $payment_method == 'CCSAVED') {
            return true;
        }
        return false;
    }
}
